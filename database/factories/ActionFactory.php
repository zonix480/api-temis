<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Action;
use Faker\Generator as Faker;

$factory->define(Action::class, function (Faker $faker) {

    return [
        'date' => $faker->word,
        'action' => $faker->word,
        'observation' => $faker->word,
        'date_start' => $faker->word,
        'date_end' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'proceedings_id' => $faker->randomDigitNotNull
    ];
});
