<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Proceeding;
use Faker\Generator as Faker;

$factory->define(Proceeding::class, function (Faker $faker) {

    return [
        'number' => $faker->word,
        'application_filing_date' => $faker->word,
        'date_acceptance_conciliator' => $faker->word,
        'date_acceptance_application' => $faker->word,
        'type_settlements' => $faker->word,
        'arbitrament' => $faker->word,
        'applicants_id' => $faker->randomDigitNotNull,
        'summoneds_id' => $faker->randomDigitNotNull,
        'proceeding_states_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->word,
        'updated_at' => $faker->word,
        'deleted_at' => $faker->word
    ];
});
