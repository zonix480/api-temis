<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Purse;
use Faker\Generator as Faker;

$factory->define(Purse::class, function (Faker $faker) {

    return [
        'passive_capital' => $faker->word,
        'reconciliation_fee' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'asesorname' => $faker->word,
        'proceedings_id' => $faker->randomDigitNotNull,
        'pursecol' => $faker->word
    ];
});
