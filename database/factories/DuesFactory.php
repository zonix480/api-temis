<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dues;
use Faker\Generator as Faker;

$factory->define(Dues::class, function (Faker $faker) {

    return [
        'due_number' => $faker->word,
        'due_value' => $faker->word,
        'date' => $faker->word,
        'observation' => $faker->text,
        'state' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'purse_id' => $faker->randomDigitNotNull
    ];
});
