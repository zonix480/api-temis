<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Summoned;
use Faker\Generator as Faker;

$factory->define(Summoned::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'identification' => $faker->word,
        'email' => $faker->word,
        'address' => $faker->word,
        'phone' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
