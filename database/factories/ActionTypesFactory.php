<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ActionTypes;
use Faker\Generator as Faker;

$factory->define(ActionTypes::class, function (Faker $faker) {

    return [
        'action_type' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
