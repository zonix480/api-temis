<?php namespace Tests\Repositories;

use App\Models\Proceeding;
use App\Repositories\ProceedingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProceedingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProceedingRepository
     */
    protected $proceedingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->proceedingRepo = \App::make(ProceedingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_proceeding()
    {
        $proceeding = factory(Proceeding::class)->make()->toArray();

        $createdProceeding = $this->proceedingRepo->create($proceeding);

        $createdProceeding = $createdProceeding->toArray();
        $this->assertArrayHasKey('id', $createdProceeding);
        $this->assertNotNull($createdProceeding['id'], 'Created Proceeding must have id specified');
        $this->assertNotNull(Proceeding::find($createdProceeding['id']), 'Proceeding with given id must be in DB');
        $this->assertModelData($proceeding, $createdProceeding);
    }

    /**
     * @test read
     */
    public function test_read_proceeding()
    {
        $proceeding = factory(Proceeding::class)->create();

        $dbProceeding = $this->proceedingRepo->find($proceeding->id);

        $dbProceeding = $dbProceeding->toArray();
        $this->assertModelData($proceeding->toArray(), $dbProceeding);
    }

    /**
     * @test update
     */
    public function test_update_proceeding()
    {
        $proceeding = factory(Proceeding::class)->create();
        $fakeProceeding = factory(Proceeding::class)->make()->toArray();

        $updatedProceeding = $this->proceedingRepo->update($fakeProceeding, $proceeding->id);

        $this->assertModelData($fakeProceeding, $updatedProceeding->toArray());
        $dbProceeding = $this->proceedingRepo->find($proceeding->id);
        $this->assertModelData($fakeProceeding, $dbProceeding->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_proceeding()
    {
        $proceeding = factory(Proceeding::class)->create();

        $resp = $this->proceedingRepo->delete($proceeding->id);

        $this->assertTrue($resp);
        $this->assertNull(Proceeding::find($proceeding->id), 'Proceeding should not exist in DB');
    }
}
