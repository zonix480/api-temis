<?php namespace Tests\Repositories;

use App\Models\ActionTypes;
use App\Repositories\ActionTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ActionTypesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActionTypesRepository
     */
    protected $actionTypesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->actionTypesRepo = \App::make(ActionTypesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->make()->toArray();

        $createdActionTypes = $this->actionTypesRepo->create($actionTypes);

        $createdActionTypes = $createdActionTypes->toArray();
        $this->assertArrayHasKey('id', $createdActionTypes);
        $this->assertNotNull($createdActionTypes['id'], 'Created ActionTypes must have id specified');
        $this->assertNotNull(ActionTypes::find($createdActionTypes['id']), 'ActionTypes with given id must be in DB');
        $this->assertModelData($actionTypes, $createdActionTypes);
    }

    /**
     * @test read
     */
    public function test_read_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->create();

        $dbActionTypes = $this->actionTypesRepo->find($actionTypes->id);

        $dbActionTypes = $dbActionTypes->toArray();
        $this->assertModelData($actionTypes->toArray(), $dbActionTypes);
    }

    /**
     * @test update
     */
    public function test_update_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->create();
        $fakeActionTypes = factory(ActionTypes::class)->make()->toArray();

        $updatedActionTypes = $this->actionTypesRepo->update($fakeActionTypes, $actionTypes->id);

        $this->assertModelData($fakeActionTypes, $updatedActionTypes->toArray());
        $dbActionTypes = $this->actionTypesRepo->find($actionTypes->id);
        $this->assertModelData($fakeActionTypes, $dbActionTypes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->create();

        $resp = $this->actionTypesRepo->delete($actionTypes->id);

        $this->assertTrue($resp);
        $this->assertNull(ActionTypes::find($actionTypes->id), 'ActionTypes should not exist in DB');
    }
}
