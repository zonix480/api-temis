<?php namespace Tests\Repositories;

use App\Models\Purse;
use App\Repositories\PurseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PurseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PurseRepository
     */
    protected $purseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->purseRepo = \App::make(PurseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_purse()
    {
        $purse = factory(Purse::class)->make()->toArray();

        $createdPurse = $this->purseRepo->create($purse);

        $createdPurse = $createdPurse->toArray();
        $this->assertArrayHasKey('id', $createdPurse);
        $this->assertNotNull($createdPurse['id'], 'Created Purse must have id specified');
        $this->assertNotNull(Purse::find($createdPurse['id']), 'Purse with given id must be in DB');
        $this->assertModelData($purse, $createdPurse);
    }

    /**
     * @test read
     */
    public function test_read_purse()
    {
        $purse = factory(Purse::class)->create();

        $dbPurse = $this->purseRepo->find($purse->id);

        $dbPurse = $dbPurse->toArray();
        $this->assertModelData($purse->toArray(), $dbPurse);
    }

    /**
     * @test update
     */
    public function test_update_purse()
    {
        $purse = factory(Purse::class)->create();
        $fakePurse = factory(Purse::class)->make()->toArray();

        $updatedPurse = $this->purseRepo->update($fakePurse, $purse->id);

        $this->assertModelData($fakePurse, $updatedPurse->toArray());
        $dbPurse = $this->purseRepo->find($purse->id);
        $this->assertModelData($fakePurse, $dbPurse->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_purse()
    {
        $purse = factory(Purse::class)->create();

        $resp = $this->purseRepo->delete($purse->id);

        $this->assertTrue($resp);
        $this->assertNull(Purse::find($purse->id), 'Purse should not exist in DB');
    }
}
