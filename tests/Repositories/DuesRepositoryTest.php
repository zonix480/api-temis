<?php namespace Tests\Repositories;

use App\Models\Dues;
use App\Repositories\DuesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DuesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DuesRepository
     */
    protected $duesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->duesRepo = \App::make(DuesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_dues()
    {
        $dues = factory(Dues::class)->make()->toArray();

        $createdDues = $this->duesRepo->create($dues);

        $createdDues = $createdDues->toArray();
        $this->assertArrayHasKey('id', $createdDues);
        $this->assertNotNull($createdDues['id'], 'Created Dues must have id specified');
        $this->assertNotNull(Dues::find($createdDues['id']), 'Dues with given id must be in DB');
        $this->assertModelData($dues, $createdDues);
    }

    /**
     * @test read
     */
    public function test_read_dues()
    {
        $dues = factory(Dues::class)->create();

        $dbDues = $this->duesRepo->find($dues->id);

        $dbDues = $dbDues->toArray();
        $this->assertModelData($dues->toArray(), $dbDues);
    }

    /**
     * @test update
     */
    public function test_update_dues()
    {
        $dues = factory(Dues::class)->create();
        $fakeDues = factory(Dues::class)->make()->toArray();

        $updatedDues = $this->duesRepo->update($fakeDues, $dues->id);

        $this->assertModelData($fakeDues, $updatedDues->toArray());
        $dbDues = $this->duesRepo->find($dues->id);
        $this->assertModelData($fakeDues, $dbDues->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_dues()
    {
        $dues = factory(Dues::class)->create();

        $resp = $this->duesRepo->delete($dues->id);

        $this->assertTrue($resp);
        $this->assertNull(Dues::find($dues->id), 'Dues should not exist in DB');
    }
}
