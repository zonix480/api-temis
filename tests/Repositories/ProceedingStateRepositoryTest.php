<?php namespace Tests\Repositories;

use App\Models\ProceedingState;
use App\Repositories\ProceedingStateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProceedingStateRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProceedingStateRepository
     */
    protected $proceedingStateRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->proceedingStateRepo = \App::make(ProceedingStateRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->make()->toArray();

        $createdProceedingState = $this->proceedingStateRepo->create($proceedingState);

        $createdProceedingState = $createdProceedingState->toArray();
        $this->assertArrayHasKey('id', $createdProceedingState);
        $this->assertNotNull($createdProceedingState['id'], 'Created ProceedingState must have id specified');
        $this->assertNotNull(ProceedingState::find($createdProceedingState['id']), 'ProceedingState with given id must be in DB');
        $this->assertModelData($proceedingState, $createdProceedingState);
    }

    /**
     * @test read
     */
    public function test_read_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->create();

        $dbProceedingState = $this->proceedingStateRepo->find($proceedingState->id);

        $dbProceedingState = $dbProceedingState->toArray();
        $this->assertModelData($proceedingState->toArray(), $dbProceedingState);
    }

    /**
     * @test update
     */
    public function test_update_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->create();
        $fakeProceedingState = factory(ProceedingState::class)->make()->toArray();

        $updatedProceedingState = $this->proceedingStateRepo->update($fakeProceedingState, $proceedingState->id);

        $this->assertModelData($fakeProceedingState, $updatedProceedingState->toArray());
        $dbProceedingState = $this->proceedingStateRepo->find($proceedingState->id);
        $this->assertModelData($fakeProceedingState, $dbProceedingState->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->create();

        $resp = $this->proceedingStateRepo->delete($proceedingState->id);

        $this->assertTrue($resp);
        $this->assertNull(ProceedingState::find($proceedingState->id), 'ProceedingState should not exist in DB');
    }
}
