<?php namespace Tests\Repositories;

use App\Models\Summoned;
use App\Repositories\SummonedRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SummonedRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SummonedRepository
     */
    protected $summonedRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->summonedRepo = \App::make(SummonedRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_summoned()
    {
        $summoned = factory(Summoned::class)->make()->toArray();

        $createdSummoned = $this->summonedRepo->create($summoned);

        $createdSummoned = $createdSummoned->toArray();
        $this->assertArrayHasKey('id', $createdSummoned);
        $this->assertNotNull($createdSummoned['id'], 'Created Summoned must have id specified');
        $this->assertNotNull(Summoned::find($createdSummoned['id']), 'Summoned with given id must be in DB');
        $this->assertModelData($summoned, $createdSummoned);
    }

    /**
     * @test read
     */
    public function test_read_summoned()
    {
        $summoned = factory(Summoned::class)->create();

        $dbSummoned = $this->summonedRepo->find($summoned->id);

        $dbSummoned = $dbSummoned->toArray();
        $this->assertModelData($summoned->toArray(), $dbSummoned);
    }

    /**
     * @test update
     */
    public function test_update_summoned()
    {
        $summoned = factory(Summoned::class)->create();
        $fakeSummoned = factory(Summoned::class)->make()->toArray();

        $updatedSummoned = $this->summonedRepo->update($fakeSummoned, $summoned->id);

        $this->assertModelData($fakeSummoned, $updatedSummoned->toArray());
        $dbSummoned = $this->summonedRepo->find($summoned->id);
        $this->assertModelData($fakeSummoned, $dbSummoned->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_summoned()
    {
        $summoned = factory(Summoned::class)->create();

        $resp = $this->summonedRepo->delete($summoned->id);

        $this->assertTrue($resp);
        $this->assertNull(Summoned::find($summoned->id), 'Summoned should not exist in DB');
    }
}
