<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Proceeding;

class ProceedingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_proceeding()
    {
        $proceeding = factory(Proceeding::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/proceedings', $proceeding
        );

        $this->assertApiResponse($proceeding);
    }

    /**
     * @test
     */
    public function test_read_proceeding()
    {
        $proceeding = factory(Proceeding::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/proceedings/'.$proceeding->id
        );

        $this->assertApiResponse($proceeding->toArray());
    }

    /**
     * @test
     */
    public function test_update_proceeding()
    {
        $proceeding = factory(Proceeding::class)->create();
        $editedProceeding = factory(Proceeding::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/proceedings/'.$proceeding->id,
            $editedProceeding
        );

        $this->assertApiResponse($editedProceeding);
    }

    /**
     * @test
     */
    public function test_delete_proceeding()
    {
        $proceeding = factory(Proceeding::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/proceedings/'.$proceeding->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/proceedings/'.$proceeding->id
        );

        $this->response->assertStatus(404);
    }
}
