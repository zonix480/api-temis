<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Dues;

class DuesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_dues()
    {
        $dues = factory(Dues::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/dues', $dues
        );

        $this->assertApiResponse($dues);
    }

    /**
     * @test
     */
    public function test_read_dues()
    {
        $dues = factory(Dues::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/dues/'.$dues->id
        );

        $this->assertApiResponse($dues->toArray());
    }

    /**
     * @test
     */
    public function test_update_dues()
    {
        $dues = factory(Dues::class)->create();
        $editedDues = factory(Dues::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/dues/'.$dues->id,
            $editedDues
        );

        $this->assertApiResponse($editedDues);
    }

    /**
     * @test
     */
    public function test_delete_dues()
    {
        $dues = factory(Dues::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/dues/'.$dues->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/dues/'.$dues->id
        );

        $this->response->assertStatus(404);
    }
}
