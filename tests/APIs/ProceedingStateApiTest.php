<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProceedingState;

class ProceedingStateApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/proceeding_states', $proceedingState
        );

        $this->assertApiResponse($proceedingState);
    }

    /**
     * @test
     */
    public function test_read_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/proceeding_states/'.$proceedingState->id
        );

        $this->assertApiResponse($proceedingState->toArray());
    }

    /**
     * @test
     */
    public function test_update_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->create();
        $editedProceedingState = factory(ProceedingState::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/proceeding_states/'.$proceedingState->id,
            $editedProceedingState
        );

        $this->assertApiResponse($editedProceedingState);
    }

    /**
     * @test
     */
    public function test_delete_proceeding_state()
    {
        $proceedingState = factory(ProceedingState::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/proceeding_states/'.$proceedingState->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/proceeding_states/'.$proceedingState->id
        );

        $this->response->assertStatus(404);
    }
}
