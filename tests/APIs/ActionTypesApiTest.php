<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ActionTypes;

class ActionTypesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/action_types', $actionTypes
        );

        $this->assertApiResponse($actionTypes);
    }

    /**
     * @test
     */
    public function test_read_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/action_types/'.$actionTypes->id
        );

        $this->assertApiResponse($actionTypes->toArray());
    }

    /**
     * @test
     */
    public function test_update_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->create();
        $editedActionTypes = factory(ActionTypes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/action_types/'.$actionTypes->id,
            $editedActionTypes
        );

        $this->assertApiResponse($editedActionTypes);
    }

    /**
     * @test
     */
    public function test_delete_action_types()
    {
        $actionTypes = factory(ActionTypes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/action_types/'.$actionTypes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/action_types/'.$actionTypes->id
        );

        $this->response->assertStatus(404);
    }
}
