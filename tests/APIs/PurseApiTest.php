<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Purse;

class PurseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_purse()
    {
        $purse = factory(Purse::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/purses', $purse
        );

        $this->assertApiResponse($purse);
    }

    /**
     * @test
     */
    public function test_read_purse()
    {
        $purse = factory(Purse::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/purses/'.$purse->id
        );

        $this->assertApiResponse($purse->toArray());
    }

    /**
     * @test
     */
    public function test_update_purse()
    {
        $purse = factory(Purse::class)->create();
        $editedPurse = factory(Purse::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/purses/'.$purse->id,
            $editedPurse
        );

        $this->assertApiResponse($editedPurse);
    }

    /**
     * @test
     */
    public function test_delete_purse()
    {
        $purse = factory(Purse::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/purses/'.$purse->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/purses/'.$purse->id
        );

        $this->response->assertStatus(404);
    }
}
