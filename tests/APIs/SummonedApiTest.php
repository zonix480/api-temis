<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Summoned;

class SummonedApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_summoned()
    {
        $summoned = factory(Summoned::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/summoneds', $summoned
        );

        $this->assertApiResponse($summoned);
    }

    /**
     * @test
     */
    public function test_read_summoned()
    {
        $summoned = factory(Summoned::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/summoneds/'.$summoned->id
        );

        $this->assertApiResponse($summoned->toArray());
    }

    /**
     * @test
     */
    public function test_update_summoned()
    {
        $summoned = factory(Summoned::class)->create();
        $editedSummoned = factory(Summoned::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/summoneds/'.$summoned->id,
            $editedSummoned
        );

        $this->assertApiResponse($editedSummoned);
    }

    /**
     * @test
     */
    public function test_delete_summoned()
    {
        $summoned = factory(Summoned::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/summoneds/'.$summoned->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/summoneds/'.$summoned->id
        );

        $this->response->assertStatus(404);
    }
}
