<?php

namespace App\Repositories;

use App\Models\Purse;
use App\Repositories\BaseRepository;

/**
 * Class PurseRepository
 * @package App\Repositories
 * @version July 22, 2021, 6:22 pm UTC
*/

class PurseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'passive_capital',
        'reconciliation_fee',
        'asesorname',
        'proceedings_id',
        'value',
        'pursecol'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purse::class;
    }
}
