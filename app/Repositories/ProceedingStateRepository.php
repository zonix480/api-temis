<?php

namespace App\Repositories;

use App\Models\ProceedingState;
use App\Repositories\BaseRepository;

/**
 * Class ProceedingStateRepository
 * @package App\Repositories
 * @version June 14, 2021, 4:10 am UTC
*/

class ProceedingStateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProceedingState::class;
    }
}
