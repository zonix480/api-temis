<?php

namespace App\Repositories;

use App\Models\Applicant;
use App\Repositories\BaseRepository;

/**
 * Class ApplicantRepository
 * @package App\Repositories
 * @version June 14, 2021, 4:09 am UTC
*/

class ApplicantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'identification',
        'email',
        'address',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Applicant::class;
    }
}
