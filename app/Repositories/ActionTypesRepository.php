<?php

namespace App\Repositories;

use App\Models\ActionTypes;
use App\Repositories\BaseRepository;

/**
 * Class ActionTypesRepository
 * @package App\Repositories
 * @version July 9, 2021, 4:31 pm UTC
*/

class ActionTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'action_type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ActionTypes::class;
    }
}
