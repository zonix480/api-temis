<?php

namespace App\Repositories;

use App\Models\Dues;
use App\Repositories\BaseRepository;

/**
 * Class DuesRepository
 * @package App\Repositories
 * @version July 22, 2021, 6:18 pm UTC
*/

class DuesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'due_number',
        'due_value',
        'date',
        'observation',
        'state',
        'purse_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dues::class;
    }
}
