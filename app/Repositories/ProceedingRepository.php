<?php

namespace App\Repositories;

use App\Models\Proceeding;
use App\Repositories\BaseRepository;

/**
 * Class ProceedingRepository
 * @package App\Repositories
 * @version June 14, 2021, 4:05 am UTC
*/

class ProceedingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'number',
        'application_filing_date',
        'date_acceptance_conciliator',
        'date_acceptance_application',
        'type_settlements',
        'arbitrament',
        'applicants_id',
        'summoneds_id',
        'proceeding_states_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Proceeding::class;
    }
}
