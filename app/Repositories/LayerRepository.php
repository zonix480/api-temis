<?php

namespace App\Repositories;

use App\Models\Layer;
use App\Repositories\BaseRepository;

/**
 * Class LayerRepository
 * @package App\Repositories
 * @version February 19, 2020, 3:37 am UTC
*/

class LayerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'image',
        'designs_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Layer::class;
    }
}
