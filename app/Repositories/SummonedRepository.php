<?php

namespace App\Repositories;

use App\Models\Summoned;
use App\Repositories\BaseRepository;

/**
 * Class SummonedRepository
 * @package App\Repositories
 * @version June 14, 2021, 4:10 am UTC
*/

class SummonedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'identification',
        'email',
        'address',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Summoned::class;
    }
}
