<?php

namespace App\Repositories;

use App\Models\Action;
use App\Repositories\BaseRepository;

/**
 * Class ActionRepository
 * @package App\Repositories
 * @version June 14, 2021, 4:10 am UTC
*/

class ActionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'action',
        'observation',
        'date_start',
        'date_end',
        'proceedings_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Action::class;
    }
}
