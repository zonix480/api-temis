<?php

namespace App\Repositories;

use App\Models\Configuration;
use App\Repositories\BaseRepository;

/**
 * Class ConfigurationRepository
 * @package App\Repositories
 * @version February 19, 2020, 3:36 am UTC
*/

class ConfigurationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Configuration::class;
    }
}
