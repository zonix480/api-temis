<?php

namespace App\DataTables;
use App\Models\Proceeding;

use App\Models\Purse;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PurseDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable->addColumn('action', 'purses.datatables_actions')->editColumn('passive_capital', function ($model) {
            if ($model->id) {
                return "$".$model->passive_capital;
            }
        })->editColumn('reconciliation_fee', function ($model) {
            if ($model->id) {
                return "$".$model->reconciliation_fee;
            }
        })->editColumn('proceedings_id', function ($model) {
            $MdProceding = new Proceeding();
            $data = $MdProceding->getProceedById($model->proceedings_id);
            return $data->isNotEmpty() ? "#".$data[0]->number : 'Sin Asignar';
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Purse $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Purse $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *z
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Cartera #' => ['name' => 'id', 'data' => 'id'],
            'Capitar Pasivo' => ['name' => 'passive_capital', 'data' => 'passive_capital'],
            'Tarifa del centro de conciliación' => ['name' => 'reconciliation_fee', 'data' => 'reconciliation_fee'],
            'Nombre del asesor' => ['name' => 'asesorname', 'data' => 'asesorname'],
            'Expediente' => ['name' => 'proceedings_id', 'data' => 'proceedings_id'],
            'Numero de Cuotas' => ['name' => 'quotas', 'data' => 'quotas'],
            'Dia de corte' => ['name' => 'pursecol', 'data' => 'pursecol'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'purses_datatable_' . time();
    }
}
