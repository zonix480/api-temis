<?php

namespace App\DataTables;

use App\Models\Applicant;
use App\Models\Proceeding;
use App\Models\ProceedingState;
use App\Models\Summoned;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Models\FormatsPHP;
use App\Models\User;

class ProceedingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'proceedings.datatables_actions')->editColumn('applicants_id', function ($model) {
            if ($model->id) {
                $mdApplicant = new Applicant();
                $data = $mdApplicant->getById($model->applicants_id);
                return $data->isNotEmpty() ? $data[0]->name.' '.$data[0]->lastname : 'Sin Asignar';
            }
        })->editColumn('summoneds_id', function ($model) {
            if ($model->id) {
                $mdApplicant = new Summoned();
                $data = $mdApplicant->getById($model->summoneds_id);
                return $data->isNotEmpty() ? $data[0]->name.' '.$data[0]->lastname : 'Sin Asignar';
            }
        })->editColumn('proceeding_states_id',function ($model){
            if ($model->id) {
                $mdApplicant = new ProceedingState();
                $data = $mdApplicant->getById($model->proceeding_states_id);
                return $data->isNotEmpty() ? $data[0]->name : 'Sin Asignar';
            }
        })->editColumn('application_filing_date', function ($model) {
            $format = new FormatsPHP();
            $date = $format->formatDate('d/m/Y', $model->application_filing_date);
            return $date;
        })->editColumn('date_acceptance_conciliator', function ($model) {
            $format = new FormatsPHP();
            $date = $format->formatDate('d/m/Y', $model->date_acceptance_conciliator);
            return $date;
        })->editColumn('date_acceptance_application', function ($model) {
            $format = new FormatsPHP();
            $date = $format->formatDate('d/m/Y', $model->date_acceptance_application);
            return $date;
        })->editColumn('conciliator_id', function ($model) {
            if ($model->id) {
                $mdApplicant = new User();
                $data = $mdApplicant->getUserById($model->conciliator_id);
                return $data->isNotEmpty() ? $data[0]->name.' '. $data[0]->lastname : 'Sin Asignar';
            }
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Proceeding $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Proceeding $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json']
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Numero' => ['name' => 'number', 'data' => 'number'],
            'Fecha de radicación' => ['name' => 'application_filing_date', 'data' => 'application_filing_date'],
            'Fecha de aceptación conciliador' => ['name' => 'date_acceptance_conciliator', 'data' => 'date_acceptance_conciliator'],
            'Fecha de aceptación de la solicitud' => ['name' => 'date_acceptance_application', 'data' => 'date_acceptance_application'],
            'Tipo de concilicación' => ['name' => 'type_settlements', 'data' => 'type_settlements'],
            'Solicitante' => ['name' => 'applicants_id', 'data' => 'applicants_id'],
            'Convocado' => ['name' => 'summoneds_id', 'data' => 'summoneds_id'],
            'Conciliador' => ['name' => 'conciliator_id', 'data' => 'conciliator_id'],
            'Estado' => ['name' => 'proceeding_states_id', 'data' => 'proceeding_states_id']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'proceedings_datatable_' . time();
    }
}
