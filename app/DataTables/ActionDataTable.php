<?php

namespace App\DataTables;

use App\Models\Action;
use App\Models\Proceeding;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ActionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'actions.datatables_actions')->editColumn('proceedings_id', function ($model) {
            $MdProceding = new Proceeding();
            $data = $MdProceding->getProceedById($model->proceedings_id);
            return $data->isNotEmpty() ? "#".$data[0]->number : 'Sin Asignar';
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Action $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Action $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Fecha' => ['name' => 'date', 'data' => 'date'],
            'Acción' => ['name' => 'action_', 'data' => 'action_'],
            'Observación' => ['name' => 'observation', 'data' => 'observation'],
            'Fecha inicio' => ['name' => 'date_start', 'data' => 'date_start'],
            'Fecha fin' => ['name' => 'date_end', 'data' => 'date_end'],
            'Procedimiento' => ['name' => 'proceedings_id', 'data' => 'proceedings_id']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'actions_datatable_' . time();
    }
}
