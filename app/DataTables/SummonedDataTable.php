<?php

namespace App\DataTables;

use App\Models\Summoned;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class SummonedDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'summoneds.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Summoned $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Summoned $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Nombres' => ['name' => 'name', 'data' => 'name'],
            'Apellidos' => ['name' => 'lastname', 'data' => 'lastname'],
            'Identificación' => ['name' => 'identification', 'data' => 'identification'],
            'Email' => ['name' => 'email', 'data' => 'email'],
            'Dirección' => ['name' => 'address', 'data' => 'address'],
            'Telefono' => ['name' => 'phone', 'data' => 'phone'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'summoneds_datatable_' . time();
    }
}
