<?php

namespace App\DataTables;

use App\Models\Dues;
use App\Models\Purse;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Carbon\Carbon;
class DuesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'dues.datatables_actions')->editColumn('due_value', function ($model) {
            if ($model->id) {
                return "$".number_format($model->due_value, 2);
            }
        })->editColumn('purse_id', function ($model) {
            $mdCol = new Purse();
            $get = $mdCol->getExpedienteByPurse($model->purse_id);
            if ($model->id) {
                return $get[0]->number;
            }
        })->editColumn('date', function ($model) {
            return $model->date->day.'-'.$model->date->month.'-'.$model->date->year;
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Dues $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Dues $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Cuota Numero' => ['name' => 'due_number', 'data' => 'due_number'],
            'Cuota Valor' => ['name' => 'due_value', 'data' => 'due_value'],
            'Fecha oportuna' => ['name' => 'date', 'data' => 'date'],
            'Fecha de pago' => ['name' => 'payment_date', 'data' => 'payment_date'],
            'Observación' => ['name' => 'observation', 'data' => 'observation'],
            'Estado' => ['name' => 'state', 'data' => 'state'],
            'Numero de expediente' => ['name' => 'number', 'data' => 'number'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'dues_datatable_' . time();
    }
}
