<?php

namespace App\Http\Controllers;

use App\DataTables\ApplicantDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateApplicantRequest;
use App\Http\Requests\UpdateApplicantRequest;
use App\Repositories\ApplicantRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Applicant;
use App\Models\Dues;
use App\Models\Purse;
use App\Models\Proceeding;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ApplicantController extends AppBaseController
{
    /** @var  ApplicantRepository */
    private $applicantRepository;

    public function __construct(ApplicantRepository $applicantRepo)
    {
        $this->applicantRepository = $applicantRepo;
    }

    /**
     * Display a listing of the Applicant.
     *
     * @param ApplicantDataTable $applicantDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ApplicantDataTable $applicantDataTable)
    {
        return $applicantDataTable->render('applicants.index');
    }

    /**
     * Show the form for creating a new Applicant.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('applicants.create');
    }

    /**
     * Store a newly created Applicant in storage.
     *
     * @param CreateApplicantRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateApplicantRequest $request)
    {
        $mdApplicants = new Applicant();
        $mdProceeding = new Proceeding();
        $mdPurse = new Purse();
        $mdDues = new Dues();
        $input = $request->all();
        $date = \Carbon\Carbon::create($input['date']);
        $input['total'] = substr( $input['total'], 1, -1);
        $input['reconciliation_fee'] =  substr($input['reconciliation_fee'], 1, -1);
        $input['total'] = substr( $input['total'], 0, -2);
        $input['reconciliation_fee'] =  substr($input['reconciliation_fee'], 0, -2);
        $input['total'] = str_replace (",","",$input['total']);
        $input['reconciliation_fee'] = str_replace (",","",$input['reconciliation_fee']);
        $input['total'] = intval ($input['total']);
        $input['reconciliation_fee'] = intval ($input['reconciliation_fee']);
        if($input['total'] / $input['quotas'] != $input['reconciliation_fee']){
            Flash::error('Revise los datos de pago ingresados no son correctos');
            return redirect(route('applicants.index'));
        }else{
            $getProceeding = $mdProceeding->getProceedByNumber($input['number']);
            if($getProceeding->isEmpty()){
                $applicant = $mdApplicants->getByIdentification($input['identification']);
                if($applicant->isEmpty()){
                    $applicant = $this->applicantRepository->create($input);
                }
                $proceeding = $mdProceeding->createProceedingBasic($input['number'], $applicant->id, $input['type_settlements']);
                //Purse = cartera
                $newPurse = $mdPurse->createPurse($input['total'], $input['reconciliation_fee'], $input['asesor'], $proceeding,$date->month, $input['quotas']);
                $mdDues->createDue(1, $input['reconciliation_fee'],$date,'pago, inicial', 'Pagado', $newPurse, $input['number']);
                for ($i=1; $i < $input['quotas'] ; $i++) { 
                    # code...
                    $datta =$date->addMonths(1);
                    $mdDues->createDue($i+1,$input['reconciliation_fee'],$datta,'aun no paga', 'No pago', $newPurse, $input['number']);
                }
                Flash::success('Solicitante guardado correctamente');
                /*header("location:https://temis.ccinmobiliario.com/temis/invoice/".$input['number']."/number/1");*/
                return redirect(route('applicants.index'));
            }else{
                Flash::error('El numero de expediente que ingreso ya existe');
                return redirect(route('applicants.index'));
            }
        }
        //print_r($input);
    }

    /**
     * Display the specified Applicant.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $applicant = $this->applicantRepository->find($id);

        if (empty($applicant)) {
            Flash::error('Solicitante no encontrado');

            return redirect(route('applicants.index'));
        }

        return view('applicants.show')->with('applicant', $applicant);
    }

    /**
     * Show the form for editing the specified Applicant.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $applicant = $this->applicantRepository->find($id);

        if (empty($applicant)) {
            Flash::error('Solicitante no encontrado');

            return redirect(route('applicants.index'));
        }

        return view('applicants.edit')->with('applicant', $applicant);
    }

    /**
     * Update the specified Applicant in storage.
     *
     * @param  int              $id
     * @param UpdateApplicantRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateApplicantRequest $request)
    {
        $applicant = $this->applicantRepository->find($id);

        if (empty($applicant)) {
            Flash::error('Solicitante no encontrado');

            return redirect(route('applicants.index'));
        }

        $applicant = $this->applicantRepository->update($request->all(), $id);

        Flash::success('Solicitante editado correctamente');

        return redirect(route('applicants.index'));
    }

    /**
     * Remove the specified Applicant from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $applicant = $this->applicantRepository->find($id);

        if (empty($applicant)) {
            Flash::error('Solicitante no encontrado');

            return redirect(route('applicants.index'));
        }

        $this->applicantRepository->delete($id);

        Flash::success('Applicant deleted successfully.');

        return redirect(route('applicants.index'));
    }
}
