<?php

namespace App\Http\Controllers;

use App\DataTables\SummonedDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSummonedRequest;
use App\Http\Requests\UpdateSummonedRequest;
use App\Repositories\SummonedRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class SummonedController extends AppBaseController
{
    /** @var  SummonedRepository */
    private $summonedRepository;

    public function __construct(SummonedRepository $summonedRepo)
    {
        $this->summonedRepository = $summonedRepo;
    }

    /**
     * Display a listing of the Summoned.
     *
     * @param SummonedDataTable $summonedDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(SummonedDataTable $summonedDataTable)
    {
        return $summonedDataTable->render('summoneds.index');
    }

    /**
     * Show the form for creating a new Summoned.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('summoneds.create');
    }

    /**
     * Store a newly created Summoned in storage.
     *
     * @param CreateSummonedRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateSummonedRequest $request)
    {
        $input = $request->all();

        $summoned = $this->summonedRepository->create($input);

        Flash::success('Convocado creado correctamente.');

        return redirect(route('summoneds.index'));
    }

    /**
     * Display the specified Summoned.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            Flash::error('Convocado no encontrado');

            return redirect(route('summoneds.index'));
        }

        return view('summoneds.show')->with('summoned', $summoned);
    }

    /**
     * Show the form for editing the specified Summoned.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            Flash::error('Convocado no encontrado');

            return redirect(route('summoneds.index'));
        }

        return view('summoneds.edit')->with('summoned', $summoned);
    }

    /**
     * Update the specified Summoned in storage.
     *
     * @param  int              $id
     * @param UpdateSummonedRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateSummonedRequest $request)
    {
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            Flash::error('Convocado no encontrado');

            return redirect(route('summoneds.index'));
        }

        $summoned = $this->summonedRepository->update($request->all(), $id);

        Flash::success('Convocado actualizado correctamente.');

        return redirect(route('summoneds.index'));
    }

    /**
     * Remove the specified Summoned from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            Flash::error('Convocado no encontrado');

            return redirect(route('summoneds.index'));
        }

        $this->summonedRepository->delete($id);

        Flash::success('Summoned deleted successfully.');

        return redirect(route('summoneds.index'));
    }
}
