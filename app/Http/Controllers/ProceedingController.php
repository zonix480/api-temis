<?php

namespace App\Http\Controllers;

use App\DataTables\ProceedingDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProceedingRequest;
use App\Http\Requests\UpdateProceedingRequest;
use App\Repositories\ProceedingRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Applicant;
use App\Models\User;
use App\Models\ProceedingState;
use App\Models\Summoned;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ProceedingController extends AppBaseController
{
    /** @var  ProceedingRepository */
    private $proceedingRepository;

    public function __construct(ProceedingRepository $proceedingRepo)
    {
        $this->proceedingRepository = $proceedingRepo;
    }

    /**
     * Display a listing of the Proceeding.
     *
     * @param ProceedingDataTable $proceedingDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ProceedingDataTable $proceedingDataTable)
    {
        return $proceedingDataTable->render('proceedings.index');
    }

    /**
     * Show the form for creating a new Proceeding.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        $mdApplicant = new Applicant();
        $applicants = $mdApplicant->get();
        $mdSummoned = new Summoned();
        $mdState = new ProceedingState();
        $mdUser = new User();
        $conciliators = $mdUser->getConciliators();
 $states = $mdState->get();
        $summoneds = $mdSummoned->get();
        return view('proceedings.create')->with('conciliators', $conciliators)->with('states', $states)->with('applicants', $applicants)->with('summoneds', $summoneds);
    }

    /**
     * Store a newly created Proceeding in storage.
     *
     * @param CreateProceedingRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateProceedingRequest $request)
    {
        $input = $request->all();

        $proceeding = $this->proceedingRepository->create($input);

        Flash::success('Expediente creado correctamente');

        return redirect(route('proceedings.index'));
    }

    /**
     * Display the specified Proceeding.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $proceeding = $this->proceedingRepository->find($id);

        if (empty($proceeding)) {
            Flash::error('Expediente no encontrado');

            return redirect(route('proceedings.index'));
        }

        return view('proceedings.show')->with('proceeding', $proceeding);
    }

    /**
     * Show the form for editing the specified Proceeding.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $proceeding = $this->proceedingRepository->find($id);
        $mdApplicant = new Applicant();
        $mdState = new ProceedingState();
        $mdSummoned = new Summoned();
        $mdUser = new User();
        $conciliators = $mdUser->getConciliators();
        $conciliatorSelected = $mdUser->getUserById($proceeding->conciliator_id);
        $applicants = $mdApplicant->get();
        $applicantSelected = $mdApplicant->getById($proceeding->applicants_id);
        $summonedSelected = $mdSummoned->getById($proceeding->summoneds_id);
        $summoneds = $mdSummoned->get();
        $states = $mdState->get();
        $stateSelected = $mdState->getById($proceeding->proceeding_states_id);
        if (empty($proceeding)) {
            Flash::error('Expediente no encontrado');

            return redirect(route('proceedings.index'));
        }

        return view('proceedings.edit')->with('conciliators', $conciliators)->with('conciliatorSelected',$conciliatorSelected)->with('stateSelected',$stateSelected)->with('states',$states)->with('proceeding', $proceeding)->with('applicants', $applicants)->with('applicantSelected', $applicantSelected)->with('summoneds', $summoneds)->with('summonedSelected', $summonedSelected);
    }

    /**
     * Update the specified Proceeding in storage.
     *
     * @param  int              $id
     * @param UpdateProceedingRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateProceedingRequest $request)
    {
        $proceeding = $this->proceedingRepository->find($id);

        if (empty($proceeding)) {
            Flash::error('Expediente no encontrado');

            return redirect(route('proceedings.index'));
        }
        
        $proceeding = $this->proceedingRepository->update($request->all(), $id);

        Flash::success('Expediente actualizado correctamente.');

        return redirect(route('proceedings.index'));
    }

    /**
     * Remove the specified Proceeding from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $proceeding = $this->proceedingRepository->find($id);

        if (empty($proceeding)) {
            Flash::error('Expediente no encontrado');

            return redirect(route('proceedings.index'));
        }

        $this->proceedingRepository->delete($id);

        Flash::success('Expediente Eliminado Correctamente.');

        return redirect(route('proceedings.index'));
    }
}
