<?php

namespace App\Http\Controllers;

use App\DataTables\PurseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePurseRequest;
use App\Http\Requests\UpdatePurseRequest;
use App\Repositories\PurseRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Dues;
use App\Models\Purse;
use Flash;
use App\Models\Proceeding;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class PurseController extends AppBaseController
{
    /** @var  PurseRepository */
    private $purseRepository;

    public function __construct(PurseRepository $purseRepo)
    {
        $this->purseRepository = $purseRepo;
    }

    /**
     * Display a listing of the Purse.
     *
     * @param PurseDataTable $purseDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(PurseDataTable $purseDataTable)
    {
        return $purseDataTable->render('purses.index');
    }

    /**
     * Show the form for creating a new Purse.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        $mdProceding = new Proceeding();
        $proceedings = $mdProceding->getProceedingsOrder();
        return view('purses.create')->with("proceedings",$proceedings);
    }

    /**
     * Store a newly created Purse in storage.
     *
     * @param CreatePurseRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreatePurseRequest $request)
    {
        $input = $request->all();
        $mdDues = new Dues();
         $input['passive_capital'] = substr( $input['passive_capital'], 1, -1);
         $input['reconciliation_fee'] =  substr($input['reconciliation_fee'], 1, -1);
         $input['passive_capital'] = substr( $input['passive_capital'], 0, -2);
         $input['reconciliation_fee'] =  substr($input['reconciliation_fee'], 0, -2);
         $input['passive_capital'] = str_replace (",","",$input['passive_capital']);
         $input['reconciliation_fee'] = str_replace (",","",$input['reconciliation_fee']);
         $input['passive_capital'] = intval ($input['passive_capital']);
         $input['reconciliation_fee'] = intval ($input['reconciliation_fee']);
        $input['value']= $input['reconciliation_fee']/$input['quotas'];
        $firstDate = \Carbon\Carbon::create($input['firstDate']);
        $purse = $this->purseRepository->create($input);
         for ($i=0; $i < $input['quotas'] ; $i++) { 
             # code...
             if($i == 0){
                $datta = $firstDate;
             }else{
                $datta =$firstDate->addMonths(1);
             }
             $mdDues->createDue($i+1, number_format($input['reconciliation_fee']/$input['quotas'], 0, '.', ''),$datta,'aun no paga', 'No pago', $purse->id);
         }
         Flash::success('Cartera creada correctamente.');

         return redirect(route('purses.index'));
    }

    /**
     * Display the specified Purse.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $purse = $this->purseRepository->find($id);

        if (empty($purse)) {
            Flash::error('Purse not found');

            return redirect(route('purses.index'));
        }

        return view('purses.show')->with('purse', $purse);
    }

    /**
     * Show the form for editing the specified Purse.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $purse = $this->purseRepository->find($id);
        $mdProceding = new Proceeding();
        $proceedings = $mdProceding->getProceedingsOrder();
        $procedingSelected = $mdProceding->getProceedByIdWithDues($purse->proceedings_id);
        if (empty($purse)) {
            Flash::error('Purse not found');

            return redirect(route('purses.index'));
        }
        return view('purses.edit')->with('purse', $purse)->with('procedingSelected', $procedingSelected)->with('proceedings', $proceedings);
    }

    /**
     * Update the specified Purse in storage.
     *
     * @param  int              $id
     * @param UpdatePurseRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdatePurseRequest $request)
    {
        $mdDues = new Dues();
        $mdPurse = new Purse();
        $input = $request->all();
        $purse = $this->purseRepository->find($id);
        $mdProceding = new Proceeding();
        $proceedingNumber = $mdProceding->getProceedById($input["proceedings_id"]);
        $delete = $mdDues->deleteDuesByNumber($proceedingNumber[0]->number);
        for ($c=1; $c <= $input["quotas"]; $c++) {
           $create = $mdDues->createDue($input["due_number_".$c], $input["due_value_".$c],$input["date_".$c],$input["observation_".$c],$input["state_".$c],$id, $proceedingNumber[0]->number);
        }

        $update = $mdPurse->updatePurse($id, $input["passive_capital"],  $input["reconciliation_fee"], $input["asesorname"], $input["proceedings_id"], 2, $input["passive_capital"],$input["quotas"]);

        Flash::success('Cartera Actualizado Correctamente.');

        return redirect(route('purses.index'));
    }

    /**
     * Remove the specified Purse from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $purse = $this->purseRepository->find($id);

        if (empty($purse)) {
            Flash::error('Purse not found');

            return redirect(route('purses.index'));
        }

        $this->purseRepository->delete($id);

        Flash::success('Purse deleted successfully.');

        return redirect(route('purses.index'));
    }
}
