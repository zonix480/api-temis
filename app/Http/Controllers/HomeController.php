<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          if (Auth::check()) {
            $mdAuth = new User();
            $currentId = $mdAuth->getUserById(Auth::user()->id);
            Session::put('currentRol', $currentId[0]->rol);
            if(Session::get('currentRol') == "admin" || Session::get('currentRol') == 'conciliador'){
                return view('home');
            }else{
                Auth::logout();    
                return view('welcome');
            }
            
        }
    }
}
