<?php

namespace App\Http\Controllers;

use App\DataTables\ActionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateActionRequest;
use App\Http\Requests\UpdateActionRequest;
use App\Repositories\ActionRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\ActionTypes;
use App\Models\Proceeding;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ActionController extends AppBaseController
{
    /** @var  ActionRepository */
    private $actionRepository;

    public function __construct(ActionRepository $actionRepo)
    {
        $this->actionRepository = $actionRepo;
    }

    /**
     * Display a listing of the Action.
     *
     * @param ActionDataTable $actionDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ActionDataTable $actionDataTable)
    {
        return $actionDataTable->render('actions.index');
    }

    /**
     * Show the form for creating a new Action.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        $mdProceding = new Proceeding();
        $proceedings = $mdProceding->getProceedingsOrder();
        $mdActionType = new ActionTypes();
        $actionTypes = $mdActionType->get();
        return view('actions.create')->with("actionTypes",$actionTypes)->with("proceedings",$proceedings);
    }

    /**
     * Store a newly created Action in storage.
     *
     * @param CreateActionRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateActionRequest $request)
    {
        $input = $request->all();
        
        $action = $this->actionRepository->create($input);

        Flash::success('Actuación guardada correctamente.');

        return redirect(route('actions.index'));
    }

    /**
     * Display the specified Action.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $action = $this->actionRepository->find($id);

        if (empty($action)) {
            Flash::error('Actuación no encontrada');

            return redirect(route('actions.index'));
        }

        return view('actions.show')->with('action', $action);
    }

    /**
     * Show the form for editing the specified Action.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $action = $this->actionRepository->find($id);
        $mdProceding = new Proceeding();
        $mdActionType = new ActionTypes();
        $actionTypes = $mdActionType->get();
        $proceedings = $mdProceding->getProceedingsOrder();
        $procedingSelected = $mdActionType->getById($action->action_type);
        $procedingnewSelected = $mdProceding->getProceedById($action->proceedings_id);
        if (empty($action)) {
            Flash::error('Action not found');

            return redirect(route('actions.index'));
        }

        return view('actions.edit')->with('action', $action)->with('actionTypes', $actionTypes)->with('procedingSelected', $procedingSelected)->with('proceedings', $proceedings)->with('procedingnewSelected', $procedingnewSelected);
    }

    /**
     * Update the specified Action in storage.
     *
     * @param  int              $id
     * @param UpdateActionRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateActionRequest $request)
    {
        $action = $this->actionRepository->find($id);

        if (empty($action)) {
            Flash::error('Action not found');

            return redirect(route('actions.index'));
        }

        $action = $this->actionRepository->update($request->all(), $id);

        Flash::success('Action updated successfully.');

        return redirect(route('actions.index'));
    }

    /**
     * Remove the specified Action from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $action = $this->actionRepository->find($id);

        if (empty($action)) {
            Flash::error('Action not found');

            return redirect(route('actions.index'));
        }

        $this->actionRepository->delete($id);

        Flash::success('Action deleted successfully.');

        return redirect(route('actions.index'));
    }
}
