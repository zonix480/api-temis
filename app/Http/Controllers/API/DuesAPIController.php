<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDuesAPIRequest;
use App\Http\Requests\API\UpdateDuesAPIRequest;
use App\Models\Dues;
use App\Repositories\DuesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DuesController
 * @package App\Http\Controllers\API
 */

class DuesAPIController extends AppBaseController
{
    /** @var  DuesRepository */
    private $duesRepository;

    public function __construct(DuesRepository $duesRepo)
    {
        $this->duesRepository = $duesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/dues",
     *      summary="Get a listing of the Dues.",
     *      tags={"Dues"},
     *      description="Get all Dues",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Dues")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $dues = $this->duesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dues->toArray(), 'Dues retrieved successfully');
    }

    /**
     * @param CreateDuesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/dues",
     *      summary="Store a newly created Dues in storage",
     *      tags={"Dues"},
     *      description="Store Dues",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Dues that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Dues")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Dues"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDuesAPIRequest $request)
    {
        $input = $request->all();

        $dues = $this->duesRepository->create($input);

        return $this->sendResponse($dues->toArray(), 'Dues saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/dues/{id}",
     *      summary="Display the specified Dues",
     *      tags={"Dues"},
     *      description="Get Dues",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Dues",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Dues"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Dues $dues */
        $dues = $this->duesRepository->find($id);

        if (empty($dues)) {
            return $this->sendError('Dues not found');
        }

        return $this->sendResponse($dues->toArray(), 'Dues retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDuesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/dues/{id}",
     *      summary="Update the specified Dues in storage",
     *      tags={"Dues"},
     *      description="Update Dues",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Dues",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Dues that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Dues")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Dues"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDuesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Dues $dues */
        $dues = $this->duesRepository->find($id);

        if (empty($dues)) {
            return $this->sendError('Dues not found');
        }

        $dues = $this->duesRepository->update($input, $id);

        return $this->sendResponse($dues->toArray(), 'Dues updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/dues/{id}",
     *      summary="Remove the specified Dues from storage",
     *      tags={"Dues"},
     *      description="Delete Dues",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Dues",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Dues $dues */
        $dues = $this->duesRepository->find($id);

        if (empty($dues)) {
            return $this->sendError('Dues not found');
        }

        $dues->delete();

        return $this->sendSuccess('Dues deleted successfully');
    }
}
