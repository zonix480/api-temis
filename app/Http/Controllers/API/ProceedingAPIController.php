<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProceedingAPIRequest;
use App\Http\Requests\API\UpdateProceedingAPIRequest;
use App\Models\Proceeding;
use App\Repositories\ProceedingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Applicant;
use App\Models\Document;
use App\Models\ProceedingState;
use App\Models\Purse;
use App\Models\Summoned;
use Response;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Mail;
use App\Mail\SendMailable;
use App\Models\User;
use Illuminate\Console\Command;
/**
 * Class ProceedingController
 * @package App\Http\Controllers\API
 */

class ProceedingAPIController extends AppBaseController
{
    /** @var  ProceedingRepository */
    private $proceedingRepository;

    public function __construct(ProceedingRepository $proceedingRepo)
    {
        $this->proceedingRepository = $proceedingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/proceedings",
     *      summary="Get a listing of the Proceedings.",
     *      tags={"Proceeding"},
     *      description="Get all Proceedings",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Proceeding")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $proceedings = $this->proceedingRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($proceedings->toArray(), 'Proceedings retrieved successfully');
    }

    /**
     * @param CreateProceedingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/proceedings",
     *      summary="Store a newly created Proceeding in storage",
     *      tags={"Proceeding"},
     *      description="Store Proceeding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Proceeding that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Proceeding")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Proceeding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProceedingAPIRequest $request)
    {
        $input = $request->all();

        $proceeding = $this->proceedingRepository->create($input);

        return $this->sendResponse($proceeding->toArray(), 'Proceeding saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/proceedings/{id}",
     *      summary="Display the specified Proceeding",
     *      tags={"Proceeding"},
     *      description="Get Proceeding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Proceeding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Proceeding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Proceeding $proceeding */
        $proceeding = $this->proceedingRepository->find($id);

        if (empty($proceeding)) {
            return $this->sendError('Proceeding not found');
        }

        return $this->sendResponse($proceeding->toArray(), 'Proceeding retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProceedingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/proceedings/{id}",
     *      summary="Update the specified Proceeding in storage",
     *      tags={"Proceeding"},
     *      description="Update Proceeding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Proceeding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Proceeding that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Proceeding")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Proceeding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProceedingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Proceeding $proceeding */
        $proceeding = $this->proceedingRepository->find($id);

        if (empty($proceeding)) {
            return $this->sendError('Proceeding not found');
        }

        $proceeding = $this->proceedingRepository->update($input, $id);

        return $this->sendResponse($proceeding->toArray(), 'Proceeding updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/proceedings/{id}",
     *      summary="Remove the specified Proceeding from storage",
     *      tags={"Proceeding"},
     *      description="Delete Proceeding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Proceeding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Proceeding $proceeding */
        $proceeding = $this->proceedingRepository->find($id);

        if (empty($proceeding)) {
            return $this->sendError('Proceeding not found');
        }

        $proceeding->delete();

        return $this->sendSuccess('Proceeding deleted successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/expediente/{id}",
     *      summary="Display the specified Proceeding",
     *      tags={"Proceeding"},
     *      description="Get Proceeding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Proceeding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="type",
     *          description="id of Proceeding",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Proceeding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function expediente($id, $type)
    {
      
        $mdProceeding = new Proceeding();
        $mdApplicant = new Applicant();
        $mdUser = new User();
        $mdProceedingState = new ProceedingState();
        $mdDocuments = new Document();
        $mdSummoned = new Summoned();
        $get = [];
        if ($type == "number") {
            $get = $mdProceeding->getProceedByNumber($id);
        } else if ($type == "cc") {
            $get = $mdProceeding->getProceedByApplicantCC($id);
            
        } else if ($type == "lastname") {
            $get = $mdProceeding->getProceedByName($id);
        }
        $final = count($get) != 0 ? $get : null;
        if ($final != null) {
            for ($c = 0; $c < count($final); $c++) {
                # code...
                $state = $mdProceedingState->getById($final[$c]->proceeding_states_id);
                $actions = $mdProceeding->getActionsByProceed($final[$c]->id);
                $applicant = $mdApplicant->getById($final[$c]->applicants_id);
                
                $summoned = $mdSummoned->getById($final[$c]->summoneds_id);
                $documents = $mdDocuments->getById($final[$c]->id);
                for ($i = 0; $i < count($documents); $i++) {
                    $documents[$i]->file = 'https://ziton-admin.ccinmobiliario.com/public/' . Storage::url($documents[$i]->name);
                }
                $final[$c]->applicant = count($applicant) != 0 ? $applicant[0] : [];
                $final[$c]->summoned =count($summoned) != 0 ? $summoned[0] : [];
                $final[$c]->state = count($state) != 0 ? $state[0] :[];
                $final[$c]->actions = $actions;
                $final[$c]->documents = $documents;
                if(isset($final[$c]->conciliator_id)){
                    $conciliator = $mdUser->getUserById($final[$c]->conciliator_id);
                    $final[$c]->conciliator = $conciliator;
                }
                
            }
        }
        /** @var Proceeding $proceeding */

        if (empty($get) || count($get) == 0) {
            return $this->sendResponse(null, 'Expediente no encontrado');
        }

        return $this->sendResponse($final, 'Proceeding retrieved successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cartera/{id}",
     *      summary="Display the specified Proceeding",
     *      tags={"Proceeding"},
     *      description="Get Proceeding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Proceeding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="type",
     *          description="id of Proceeding",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Proceeding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function cartera($id)
    {
        $mdPurse = new Purse();
        $final = $mdPurse->getByProceeding($id);
        if (empty($final) || count($final) == 0) {
            return $this->sendResponse(null, 'Expediente no encontrado');
        }
        $quotas = $mdPurse->getQuotas($final[0]->id);
        $final[0]->quotas =  $quotas;
        return $this->sendResponse($final, 'Proceeding retrieved successfully');
    }
}
