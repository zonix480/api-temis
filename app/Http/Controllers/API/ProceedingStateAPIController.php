<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProceedingStateAPIRequest;
use App\Http\Requests\API\UpdateProceedingStateAPIRequest;
use App\Models\ProceedingState;
use App\Repositories\ProceedingStateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProceedingStateController
 * @package App\Http\Controllers\API
 */

class ProceedingStateAPIController extends AppBaseController
{
    /** @var  ProceedingStateRepository */
    private $proceedingStateRepository;

    public function __construct(ProceedingStateRepository $proceedingStateRepo)
    {
        $this->proceedingStateRepository = $proceedingStateRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/proceedingStates",
     *      summary="Get a listing of the ProceedingStates.",
     *      tags={"ProceedingState"},
     *      description="Get all ProceedingStates",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ProceedingState")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $proceedingStates = $this->proceedingStateRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($proceedingStates->toArray(), 'Proceeding States retrieved successfully');
    }

    /**
     * @param CreateProceedingStateAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/proceedingStates",
     *      summary="Store a newly created ProceedingState in storage",
     *      tags={"ProceedingState"},
     *      description="Store ProceedingState",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProceedingState that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProceedingState")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProceedingState"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProceedingStateAPIRequest $request)
    {
        $input = $request->all();

        $proceedingState = $this->proceedingStateRepository->create($input);

        return $this->sendResponse($proceedingState->toArray(), 'Proceeding State saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/proceedingStates/{id}",
     *      summary="Display the specified ProceedingState",
     *      tags={"ProceedingState"},
     *      description="Get ProceedingState",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProceedingState",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProceedingState"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ProceedingState $proceedingState */
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            return $this->sendError('Proceeding State not found');
        }

        return $this->sendResponse($proceedingState->toArray(), 'Proceeding State retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProceedingStateAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/proceedingStates/{id}",
     *      summary="Update the specified ProceedingState in storage",
     *      tags={"ProceedingState"},
     *      description="Update ProceedingState",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProceedingState",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProceedingState that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProceedingState")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProceedingState"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProceedingStateAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProceedingState $proceedingState */
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            return $this->sendError('Proceeding State not found');
        }

        $proceedingState = $this->proceedingStateRepository->update($input, $id);

        return $this->sendResponse($proceedingState->toArray(), 'ProceedingState updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/proceedingStates/{id}",
     *      summary="Remove the specified ProceedingState from storage",
     *      tags={"ProceedingState"},
     *      description="Delete ProceedingState",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProceedingState",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ProceedingState $proceedingState */
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            return $this->sendError('Proceeding State not found');
        }

        $proceedingState->delete();

        return $this->sendSuccess('Proceeding State deleted successfully');
    }
}
