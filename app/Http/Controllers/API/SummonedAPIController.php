<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSummonedAPIRequest;
use App\Http\Requests\API\UpdateSummonedAPIRequest;
use App\Models\Summoned;
use App\Repositories\SummonedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SummonedController
 * @package App\Http\Controllers\API
 */

class SummonedAPIController extends AppBaseController
{
    /** @var  SummonedRepository */
    private $summonedRepository;

    public function __construct(SummonedRepository $summonedRepo)
    {
        $this->summonedRepository = $summonedRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/summoneds",
     *      summary="Get a listing of the Summoneds.",
     *      tags={"Summoned"},
     *      description="Get all Summoneds",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Summoned")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $summoneds = $this->summonedRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($summoneds->toArray(), 'Summoneds retrieved successfully');
    }

    /**
     * @param CreateSummonedAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/summoneds",
     *      summary="Store a newly created Summoned in storage",
     *      tags={"Summoned"},
     *      description="Store Summoned",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Summoned that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Summoned")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Summoned"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSummonedAPIRequest $request)
    {
        $input = $request->all();

        $summoned = $this->summonedRepository->create($input);

        return $this->sendResponse($summoned->toArray(), 'Summoned saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/summoneds/{id}",
     *      summary="Display the specified Summoned",
     *      tags={"Summoned"},
     *      description="Get Summoned",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Summoned",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Summoned"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Summoned $summoned */
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            return $this->sendError('Summoned not found');
        }

        return $this->sendResponse($summoned->toArray(), 'Summoned retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSummonedAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/summoneds/{id}",
     *      summary="Update the specified Summoned in storage",
     *      tags={"Summoned"},
     *      description="Update Summoned",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Summoned",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Summoned that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Summoned")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Summoned"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSummonedAPIRequest $request)
    {
        $input = $request->all();

        /** @var Summoned $summoned */
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            return $this->sendError('Summoned not found');
        }

        $summoned = $this->summonedRepository->update($input, $id);

        return $this->sendResponse($summoned->toArray(), 'Summoned updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/summoneds/{id}",
     *      summary="Remove the specified Summoned from storage",
     *      tags={"Summoned"},
     *      description="Delete Summoned",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Summoned",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Summoned $summoned */
        $summoned = $this->summonedRepository->find($id);

        if (empty($summoned)) {
            return $this->sendError('Summoned not found');
        }

        $summoned->delete();

        return $this->sendSuccess('Summoned deleted successfully');
    }
}
