<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePurseAPIRequest;
use App\Http\Requests\API\UpdatePurseAPIRequest;
use App\Models\Purse;
use App\Repositories\PurseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PurseController
 * @package App\Http\Controllers\API
 */

class PurseAPIController extends AppBaseController
{
    /** @var  PurseRepository */
    private $purseRepository;

    public function __construct(PurseRepository $purseRepo)
    {
        $this->purseRepository = $purseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/purses",
     *      summary="Get a listing of the Purses.",
     *      tags={"Purse"},
     *      description="Get all Purses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Purse")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $purses = $this->purseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($purses->toArray(), 'Purses retrieved successfully');
    }

    /**
     * @param CreatePurseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/purses",
     *      summary="Store a newly created Purse in storage",
     *      tags={"Purse"},
     *      description="Store Purse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Purse that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Purse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePurseAPIRequest $request)
    {
        $input = $request->all();

        $purse = $this->purseRepository->create($input);

        return $this->sendResponse($purse->toArray(), 'Purse saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/purses/{id}",
     *      summary="Display the specified Purse",
     *      tags={"Purse"},
     *      description="Get Purse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Purse $purse */
        $purse = $this->purseRepository->find($id);

        if (empty($purse)) {
            return $this->sendError('Purse not found');
        }

        return $this->sendResponse($purse->toArray(), 'Purse retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePurseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/purses/{id}",
     *      summary="Update the specified Purse in storage",
     *      tags={"Purse"},
     *      description="Update Purse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Purse that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Purse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePurseAPIRequest $request)
    {
        $input = $request->all();

        /** @var Purse $purse */
        $purse = $this->purseRepository->find($id);

        if (empty($purse)) {
            return $this->sendError('Purse not found');
        }

        $purse = $this->purseRepository->update($input, $id);

        return $this->sendResponse($purse->toArray(), 'Purse updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/purses/{id}",
     *      summary="Remove the specified Purse from storage",
     *      tags={"Purse"},
     *      description="Delete Purse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Purse $purse */
        $purse = $this->purseRepository->find($id);

        if (empty($purse)) {
            return $this->sendError('Purse not found');
        }

        $purse->delete();

        return $this->sendSuccess('Purse deleted successfully');
    }
}
