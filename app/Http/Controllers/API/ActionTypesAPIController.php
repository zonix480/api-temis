<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateActionTypesAPIRequest;
use App\Http\Requests\API\UpdateActionTypesAPIRequest;
use App\Models\ActionTypes;
use App\Repositories\ActionTypesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ActionTypesController
 * @package App\Http\Controllers\API
 */

class ActionTypesAPIController extends AppBaseController
{
    /** @var  ActionTypesRepository */
    private $actionTypesRepository;

    public function __construct(ActionTypesRepository $actionTypesRepo)
    {
        $this->actionTypesRepository = $actionTypesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/actionTypes",
     *      summary="Get a listing of the ActionTypes.",
     *      tags={"ActionTypes"},
     *      description="Get all ActionTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ActionTypes")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $actionTypes = $this->actionTypesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($actionTypes->toArray(), 'Action Types retrieved successfully');
    }

    /**
     * @param CreateActionTypesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/actionTypes",
     *      summary="Store a newly created ActionTypes in storage",
     *      tags={"ActionTypes"},
     *      description="Store ActionTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ActionTypes that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ActionTypes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ActionTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateActionTypesAPIRequest $request)
    {
        $input = $request->all();

        $actionTypes = $this->actionTypesRepository->create($input);

        return $this->sendResponse($actionTypes->toArray(), 'Action Types saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/actionTypes/{id}",
     *      summary="Display the specified ActionTypes",
     *      tags={"ActionTypes"},
     *      description="Get ActionTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ActionTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ActionTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ActionTypes $actionTypes */
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            return $this->sendError('Action Types not found');
        }

        return $this->sendResponse($actionTypes->toArray(), 'Action Types retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateActionTypesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/actionTypes/{id}",
     *      summary="Update the specified ActionTypes in storage",
     *      tags={"ActionTypes"},
     *      description="Update ActionTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ActionTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ActionTypes that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ActionTypes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ActionTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateActionTypesAPIRequest $request)
    {
        $input = $request->all();

        /** @var ActionTypes $actionTypes */
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            return $this->sendError('Action Types not found');
        }

        $actionTypes = $this->actionTypesRepository->update($input, $id);

        return $this->sendResponse($actionTypes->toArray(), 'ActionTypes updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/actionTypes/{id}",
     *      summary="Remove the specified ActionTypes from storage",
     *      tags={"ActionTypes"},
     *      description="Delete ActionTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ActionTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ActionTypes $actionTypes */
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            return $this->sendError('Action Types not found');
        }

        $actionTypes->delete();

        return $this->sendSuccess('Action Types deleted successfully');
    }
}
