<?php

namespace App\Http\Controllers;

use App\DataTables\DocumentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Repositories\DocumentRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\FormatsPHP;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use App\Models\Proceeding;
use Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DocumentController extends AppBaseController
{
    /** @var  DocumentRepository */
    private $documentRepository;

    public function __construct(DocumentRepository $documentRepo)
    {
        $this->documentRepository = $documentRepo;
    }

    /**
     * Display a listing of the Document.
     *
     * @param DocumentDataTable $documentDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(DocumentDataTable $documentDataTable)
    {
        return $documentDataTable->render('documents.index');
    }

    /**
     * Show the form for creating a new Document.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    { $mdProceding = new Proceeding();
        $proceedings = $mdProceding->getProceedings();
        return view('documents.create')->with("proceedings",$proceedings);;
    }

    /**
     * Store a newly created Document in storage.
     *
     * @param CreateDocumentRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateDocumentRequest $request)
    {
        $input = $request->all();
        //obtenemos el campo file definido en el formulario
        $file = $request->file('name');
        //indicamos que queremos guardar un nuevo archivo en el disco local
      
        $disco = Storage::disk('local')->put('files', $file);
        $input['name']= $disco;
        $document = $this->documentRepository->create($input);
        Flash::success('Document saved successfully.');
        return redirect(route('documents.index'));
    }

    /**
     * Display the specified Document.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $document = $this->documentRepository->find($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.show')->with('document', $document);
    }

    /**
     * Show the form for editing the specified Document.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $document = $this->documentRepository->find($id);
        $mdProceding = new Proceeding();
        $proceedings = $mdProceding->getProceedings();
        $procedingSelected = $mdProceding->getProceedById($document->proceedings_id);
        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.edit')->with('document', $document)->with('procedingSelected', $procedingSelected)->with('proceedings', $proceedings);
    }

    /**
     * Update the specified Document in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateDocumentRequest $request)
    {
        $document = $this->documentRepository->find($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        $document = $this->documentRepository->update($request->all(), $id);

        Flash::success('Document updated successfully.');

        return redirect(route('documents.index'));
    }

    /**
     * Remove the specified Document from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $document = $this->documentRepository->find($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        $this->documentRepository->delete($id);

        Flash::success('Document deleted successfully.');

        return redirect(route('documents.index'));
    }
}
