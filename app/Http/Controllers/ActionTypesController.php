<?php

namespace App\Http\Controllers;

use App\DataTables\ActionTypesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateActionTypesRequest;
use App\Http\Requests\UpdateActionTypesRequest;
use App\Repositories\ActionTypesRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ActionTypesController extends AppBaseController
{
    /** @var  ActionTypesRepository */
    private $actionTypesRepository;

    public function __construct(ActionTypesRepository $actionTypesRepo)
    {
        $this->actionTypesRepository = $actionTypesRepo;
    }

    /**
     * Display a listing of the ActionTypes.
     *
     * @param ActionTypesDataTable $actionTypesDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ActionTypesDataTable $actionTypesDataTable)
    {
        return $actionTypesDataTable->render('action_types.index');
    }

    /**
     * Show the form for creating a new ActionTypes.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('action_types.create');
    }

    /**
     * Store a newly created ActionTypes in storage.
     *
     * @param CreateActionTypesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateActionTypesRequest $request)
    {
        $input = $request->all();

        $actionTypes = $this->actionTypesRepository->create($input);

        Flash::success('Tipo de actuación creada correctamente');

        return redirect(route('actionTypes.index'));
    }

    /**
     * Display the specified ActionTypes.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            Flash::error('Tipo de actuación no encontrado');

            return redirect(route('actionTypes.index'));
        }

        return view('action_types.show')->with('actionTypes', $actionTypes);
    }

    /**
     * Show the form for editing the specified ActionTypes.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            Flash::error('Tipo de actuación no encontrado');

            return redirect(route('actionTypes.index'));
        }

        return view('action_types.edit')->with('actionTypes', $actionTypes);
    }

    /**
     * Update the specified ActionTypes in storage.
     *
     * @param  int              $id
     * @param UpdateActionTypesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateActionTypesRequest $request)
    {
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            Flash::error('Tipo de actuación no encontrado');

            return redirect(route('actionTypes.index'));
        }

        $actionTypes = $this->actionTypesRepository->update($request->all(), $id);

        Flash::success('Tipo de actuación actualizada correctamente');

        return redirect(route('actionTypes.index'));
    }

    /**
     * Remove the specified ActionTypes from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $actionTypes = $this->actionTypesRepository->find($id);

        if (empty($actionTypes)) {
            Flash::error('Tipo de actuación no encontrado');

            return redirect(route('actionTypes.index'));
        }

        $this->actionTypesRepository->delete($id);

        Flash::success('Tipo de actuación eliminada correctamente.');

        return redirect(route('actionTypes.index'));
    }
}
