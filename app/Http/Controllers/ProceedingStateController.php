<?php

namespace App\Http\Controllers;

use App\DataTables\ProceedingStateDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProceedingStateRequest;
use App\Http\Requests\UpdateProceedingStateRequest;
use App\Repositories\ProceedingStateRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ProceedingStateController extends AppBaseController
{
    /** @var  ProceedingStateRepository */
    private $proceedingStateRepository;

    public function __construct(ProceedingStateRepository $proceedingStateRepo)
    {
        $this->proceedingStateRepository = $proceedingStateRepo;
    }

    /**
     * Display a listing of the ProceedingState.
     *
     * @param ProceedingStateDataTable $proceedingStateDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ProceedingStateDataTable $proceedingStateDataTable)
    {
        return $proceedingStateDataTable->render('proceeding_states.index');
    }

    /**
     * Show the form for creating a new ProceedingState.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('proceeding_states.create');
    }

    /**
     * Store a newly created ProceedingState in storage.
     *
     * @param CreateProceedingStateRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateProceedingStateRequest $request)
    {
        $input = $request->all();

        $proceedingState = $this->proceedingStateRepository->create($input);

        Flash::success('Proceeding State saved successfully.');

        return redirect(route('proceedingStates.index'));
    }

    /**
     * Display the specified ProceedingState.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            Flash::error('Proceeding State not found');

            return redirect(route('proceedingStates.index'));
        }

        return view('proceeding_states.show')->with('proceedingState', $proceedingState);
    }

    /**
     * Show the form for editing the specified ProceedingState.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            Flash::error('Proceeding State not found');

            return redirect(route('proceedingStates.index'));
        }

        return view('proceeding_states.edit')->with('proceedingState', $proceedingState);
    }

    /**
     * Update the specified ProceedingState in storage.
     *
     * @param  int              $id
     * @param UpdateProceedingStateRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateProceedingStateRequest $request)
    {
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            Flash::error('Proceeding State not found');

            return redirect(route('proceedingStates.index'));
        }

        $proceedingState = $this->proceedingStateRepository->update($request->all(), $id);

        Flash::success('Proceeding State updated successfully.');

        return redirect(route('proceedingStates.index'));
    }

    /**
     * Remove the specified ProceedingState from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $proceedingState = $this->proceedingStateRepository->find($id);

        if (empty($proceedingState)) {
            Flash::error('Proceeding State not found');

            return redirect(route('proceedingStates.index'));
        }

        $this->proceedingStateRepository->delete($id);

        Flash::success('Proceeding State deleted successfully.');

        return redirect(route('proceedingStates.index'));
    }
}
