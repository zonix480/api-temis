<?php

namespace App\Http\Controllers;

use App\DataTables\ApplicantDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateApplicantRequest;
use App\Http\Requests\UpdateApplicantRequest;
use App\Repositories\ApplicantRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Applicant;
use App\Models\Dues;
use App\Models\Purse;
use App\Models\Proceeding;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class AnalyticsController extends AppBaseController
{
    /** @var  ApplicantRepository */
    private $applicantRepository;

    public function __construct(ApplicantRepository $applicantRepo)
    {
        $this->applicantRepository = $applicantRepo;
    }

    /**
     * Display a listing of the Applicant.
     *
     * @param ApplicantDataTable $applicantDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ApplicantDataTable $applicantDataTable)
    {

        $mdDue = new Dues();
        $dues = $mdDue->getDuesPayment();
        $duesforpaymentcount = $mdDue->getDuesforPaymentCount();
        $duespay = $mdDue->getDuesPayLastMonth();
        return $applicantDataTable->render('analytics.index')->with('dues', $dues)->with('duesforpayment', $duesforpaymentcount)->with('duespaylastmonth', $duespay);
    }

}
