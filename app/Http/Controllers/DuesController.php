<?php

namespace App\Http\Controllers;

use App\DataTables\DuesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDuesRequest;
use App\Http\Requests\UpdateDuesRequest;
use App\Repositories\DuesRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Proceeding;
use App\Models\Purse;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class DuesController extends AppBaseController
{
    /** @var  DuesRepository */
    private $duesRepository;

    public function __construct(DuesRepository $duesRepo)
    {
        $this->duesRepository = $duesRepo;
    }

    /**
     * Display a listing of the Dues.
     *
     * @param DuesDataTable $duesDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(DuesDataTable $duesDataTable)
    {
        return $duesDataTable->render('dues.index');
    }

    /**
     * Show the form for creating a new Dues.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        $mdPurse = new Purse();
        $purses = $mdPurse->getPurses();
        return view('dues.create')->with("purses",$purses);
    }

    /**
     * Store a newly created Dues in storage.
     *
     * @param CreateDuesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateDuesRequest $request)
    {
        $input = $request->all();

        $dues = $this->duesRepository->create($input);

        Flash::success('Dues saved successfully.');

        return redirect(route('dues.index'));
    }

    /**
     * Display the specified Dues.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $dues = $this->duesRepository->find($id);

        if (empty($dues)) {
            Flash::error('Dues not found');

            return redirect(route('dues.index'));
        }

        return view('dues.show')->with('dues', $dues);
    }

    /**
     * Show the form for editing the specified Dues.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $dues = $this->duesRepository->find($id);
        $mdPurse = new Purse();
        $purses = $mdPurse->getPurses();
        $purseSelected = $mdPurse->getPurseById($dues->purse_id);
        if (empty($dues)) {
            Flash::error('Dues not found');

            return redirect(route('dues.index'));
        }

        return view('dues.edit')->with('dues', $dues)->with('purseSelected', $purseSelected)->with('purses', $purses);
    }

    /**
     * Update the specified Dues in storage.
     *
     * @param  int              $id
     * @param UpdateDuesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateDuesRequest $request)
    {
        $mdPurse = new Purse();
        $mdProceding = new Proceeding();
        $dues = $this->duesRepository->find($id);
        $input = $request->all();
        if (empty($dues)) {
            Flash::error('Dues not found');

            return redirect(route('dues.index'));
        }
        
        $dues = $this->duesRepository->update($request->all(), $id);
        if($input['state'] == "Pagado"){
            $getPurse = $mdPurse->getPurseById($input['purse_id']);
            $getProceding = $mdProceding->getProceedById($getPurse[0]->proceedings_id);
            $numberProceding = $getProceding[0]->number;
            $due = $input['due_number'];
            echo("<script>window.location.href = 'https://ziton.ccinmobiliario.com/invoice/$numberProceding/number/$due';</script>");
        }else{
            Flash::success('Cuota actualizada correctamente.');

            return redirect(route('dues.index'));
        }

    }

    /**
     * Remove the specified Dues from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $dues = $this->duesRepository->find($id);

        if (empty($dues)) {
            Flash::error('Dues not found');

            return redirect(route('dues.index'));
        }

        $this->duesRepository->delete($id);

        Flash::success('Dues deleted successfully.');

        return redirect(route('dues.index'));
    }
}
