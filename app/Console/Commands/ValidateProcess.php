<?php

namespace App\Console\Commands;

use App\Models\Proceeding;
use App\Models\Purse;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Mail;
use DB;
use Illuminate\Http\Request;
use App\Mail\SendMailable;

class ValidateProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'proceedings:find';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $mdPurse = new Purse();
        $get = $mdPurse->getPurses();
        $now = Carbon::now();
        $mdProceeding = new Proceeding();
        foreach ($get as $purse) {
            $quotas = $mdPurse->getQuotas($purse->id);
            $quotasNoPagas = 0;
            foreach ($quotas as $quota) {
                $date = Carbon::create($quota->date);
                if ($date <= $now && $quota->payment_date == null) {
                    $quotasNoPagas = $quotasNoPagas + 1;
                }
            }
            if ($quotasNoPagas != 0) {
                $proceeding = $mdProceeding->getProceedById($purse->proceedings_id);
                Mail::to('juliancruz300@gmail.com')->send(new SendMailable(strval($proceeding[0]->number)));
                Mail::to('notificacionesfal@gmail.com')->send(new SendMailable(strval($proceeding[0]->number)));
            }
        }
        
    }
}
