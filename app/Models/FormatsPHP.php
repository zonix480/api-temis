<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FormatsPHP
 * @package App\Models
 * @version July 23, 2018, 2:55 pm UTC
 */

class FormatsPHP extends Model
{
    use SoftDeletes;

    /**
     * @return Formats / PHP
     **/
    public static function formatDate($format, $date) {     
        setlocale(LC_ALL,"es_ES");   
        date_default_timezone_set('America/Bogota');
        return !is_null($format) && !is_null($date) ? date($format, strtotime($date) ) : '';         
    }

    public static function getToken($length) {
        $token = "";
        $code = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code.= "abcdefghijklmnopqrstuvwxyz";
        $code.= "0123456789";
        $max = strlen($code);
        for ($i=0; $i < $length; $i++) {
            $token .= $code[random_int(0, $max-1)];
        }
        return $token;
    }

    public static function findNearDate($array, $date) {
        //$count = 0;
        foreach($array as $day) {
            //$interval[$count] = abs(strtotime($date) - strtotime($day));
            $interval[] = abs(strtotime($date) - strtotime($day));
            //$count++;
        }

        asort($interval);
        $closest = key($interval);

        return $array[$closest];
    }


}
