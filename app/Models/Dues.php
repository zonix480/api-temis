<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="Dues",
 *      required={"observation", "created_at", "purse_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="due_number",
 *          description="due_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="due_value",
 *          description="due_value",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="observation",
 *          description="observation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="purse_id",
 *          description="purse_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Dues extends Model
{
    use SoftDeletes;

    public $table = 'dues';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'due_number',
        'due_value',
        'date',
        'observation',
        'state',
        'purse_id',
        'payment_date',
        'payment_type',
        'number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'due_number' => 'string',
        'due_value' => 'string',
        'date' => 'date',
        'payment_date' => 'payment_date',
        'observation' => 'string',
        'state' => 'string',
        'purse_id' => 'integer',
        'payment_type' => 'string',
        'number'=> 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'observation' => 'required',
        //'purse_id' => 'required'
    ];

    public function getDuesPayment()
    {
        $now  =  \Carbon\Carbon::now();
        return DB::table('dues')
        ->where([
            ['dues.state', '=', "Pagado"],
            ['dues.payment_date', '>=', $now->year."-01-01"],
            ['dues.payment_date', '<', $now->year."-12-31"],
        ])
        ->select('dues.*')
        ->get();
    }

    public function deleteDuesByNumber($number)
    {
        return DB::table('dues')
        ->where([
            ['dues.number', '=', $number],
        ])
        ->delete();
    }

    
    public function getDuesforPaymentCount()
    {
        $now  =  \Carbon\Carbon::now();
        return DB::table('dues')
        ->where([
            ['dues.state', '!=', "Pagado"],
        ])
        ->select('dues.*')
        ->get();
    }


    public function getDuesPayLastMonth()
    {
        $now  =  \Carbon\Carbon::now();
        return DB::table('dues')
        ->where([
            ['dues.state', '=', "Pagado"],
            ['dues.payment_date', '>=', $now->year."-".($now->month)."-01"],
            ['dues.payment_date', '<=', $now->year."-".($now->month)."-31"],
            // Cambiar para datos TOTALES ['dues.payment_date', '>=', "2021-1-01"],
            // Cambiar para datos TOTALES ['dues.payment_date', '<=', "2022-12-31"],
        ])
        ->select('dues.*')
        ->get();
    }


    public function createDue($number, $value, $date, $observation, $state, $purse_id, $proceeding)
    {
        $id = DB::table('dues')->insertGetId(
            array(
                'due_number' => $number, 'due_value' => $value, 'date' => $date,
                'observation' => $observation, 'state' => $state,
                'purse_id' => $purse_id,
                'number' => $proceeding,
                'created_at' =>  \Carbon\Carbon::now()
            )
        );
    }
    
}
