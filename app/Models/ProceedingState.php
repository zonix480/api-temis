<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="ProceedingState",
 *      required={"name", "description", "created_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ProceedingState extends Model
{
    use SoftDeletes;

    public $table = 'proceeding_states';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function proceedings()
    {
        return $this->hasMany(\App\Models\Proceedings::class, 'proceeding_states_id');
    }

    public function getById($id)
    {
            return DB::table('proceeding_states')
                ->where([
                    ['proceeding_states.id', '=', $id]
                ])
                ->whereNull('proceeding_states.deleted_at')
                ->select('proceeding_states.*')
                ->get();
    }

    public function get()
    {
            return DB::table('proceeding_states')
                ->whereNull('proceeding_states.deleted_at')
                ->select('proceeding_states.*')
                ->get();
    }
}
