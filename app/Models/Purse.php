<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="Purse",
 *      required={"passive_capital", "reconciliation_fee", "created_at", "asesorname", "proceedings_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="passive_capital",
 *          description="passive_capital",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="reconciliation_fee",
 *          description="reconciliation_fee",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="asesorname",
 *          description="asesorname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="proceedings_id",
 *          description="proceedings_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pursecol",
 *          description="pursecol",
 *          type="string"
 *      )
 * )
 */
class Purse extends Model
{
    use SoftDeletes;

    public $table = 'purse';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'passive_capital',
        'reconciliation_fee',
        'asesorname',
        'proceedings_id',
        'pursecol',
        'value',
        'quotas'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'passive_capital' => 'string',
        'reconciliation_fee' => 'string',
        'asesorname' => 'string',
        'proceedings_id' => 'integer',
        'pursecol' => 'integer',
        'value'=>'integer',
        'quotas' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'passive_capital' => 'required',
        'reconciliation_fee' => 'required',
        'asesorname' => 'required',
        'proceedings_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function proceedings()
    {
        return $this->belongsTo(\App\Models\Proceedings::class, 'proceedings_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function dues()
    {
        return $this->hasMany(\App\Models\Due::class, 'purse_id');
    }

    public function getByProceeding($proceeding)
    {

        return DB::table('proceedings')
            ->join('purse', 'proceedings.id', '=', 'purse.proceedings_id')
            ->where([
                ['proceedings.number', '=', $proceeding]
            ])
            ->select('purse.*')
            ->get();
    }
    
    public function getExpedienteByPurse($purse)
    {

        return DB::table('purse')
            ->join('proceedings', 'proceedings.id', '=', 'purse.proceedings_id')
            ->where([
                ['purse.id', '=', $purse]
            ])
            ->select('proceedings.*')
            ->get();
    }


    public function getQuotas($purse)
    {

        return DB::table('dues')
            ->where([
                ['dues.purse_id', '=', $purse]
            ])
            ->select('dues.*')
            ->get();
    }


    public function getPurses()
    {

        return DB::table('purse')
            ->select('purse.*')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function getPurseById($id)
    {

        return DB::table('purse')
        ->where([
            ['purse.id', '=', $id]
        ])
            ->select('purse.*')
            ->get();
    }



    
    public function createPurse($total, $valorCuota, $asesor, $proceedings_id, $pursecol, $quotas)
    {
        $id = DB::table('purse')->insertGetId(
            array(
                'passive_capital'=>$total,
                'reconciliation_fee'=>$total,
                'value'=>$valorCuota,
                'asesorname'=>$asesor,
                'proceedings_id'=>$proceedings_id,
                'pursecol'=>$pursecol,
                'quotas'=>$quotas,
                'created_at' =>  \Carbon\Carbon::now()
            )
        );
        return $id;
    }

    public function updatePurse($id, $passive_capital, $reconciliation_fee, $asesor, $proceedings_id, $pursecol, $value, $quotas)
    {
        return DB::table('purse')
            ->where('id', $id)
            ->update(['passive_capital' => $passive_capital,
            'reconciliation_fee' => $reconciliation_fee,
            'asesorname' => $asesor,
            'proceedings_id' => $proceedings_id,
            'pursecol' => $pursecol,
            'value' => $value,
            'quotas' => $quotas,
            ],);
    }

    
}
