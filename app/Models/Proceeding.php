<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="Proceeding",
 *      required={"number", "application_filing_date", "date_acceptance_conciliator", "date_acceptance_application", "type_settlements", "arbitrament", "applicants_id", "summoneds_id", "proceeding_states_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="number",
 *          description="number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="application_filing_date",
 *          description="application_filing_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="date_acceptance_conciliator",
 *          description="date_acceptance_conciliator",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="date_acceptance_application",
 *          description="date_acceptance_application",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="type_settlements",
 *          description="type_settlements",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="arbitrament",
 *          description="arbitrament",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="applicants_id",
 *          description="applicants_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="summoneds_id",
 *          description="summoneds_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="proceeding_states_id",
 *          description="proceeding_states_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string"
 *      )
 * )
 */
class Proceeding extends Model
{
    use SoftDeletes;

    public $table = 'proceedings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'number',
        'application_filing_date',
        'date_acceptance_conciliator',
        'date_acceptance_application',
        'type_settlements',
        'arbitrament',
        'conciliator_id',
        'applicants_id',
        'summoneds_id',
        'proceeding_states_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'number' => 'string',
        'application_filing_date' => 'date',
        'date_acceptance_conciliator' => 'date',
        'date_acceptance_application' => 'date',
        'type_settlements' => 'string',
        'arbitrament' => 'string',
        'applicants_id' => 'integer',
        'conciliator_id' => 'integer',
        'summoneds_id' => 'integer',
        'proceeding_states_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'number' => 'required',
        //'application_filing_date' => 'required',
        //'date_acceptance_conciliator' => 'required',
        //'date_acceptance_application' => 'required',
        'type_settlements' => 'required',
        //'arbitrament' => 'required',
        //'applicants_id' => 'required',
        //'summoneds_id' => 'required',
        'proceeding_states_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function applicants()
    {
        return $this->belongsTo(\App\Models\Applicant::class, 'applicants_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function proceedingStates()
    {
        return $this->belongsTo(\App\Models\ProceedingState::class, 'proceeding_states_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function summoneds()
    {
        return $this->belongsTo(\App\Models\Summoned::class, 'summoneds_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function actions()
    {
        return $this->hasMany(\App\Models\Action::class, 'proceedings_id');
    }


    public function createProceedingBasic($number, $applicantId, $type)
    {
        $id = DB::table('proceedings')->insertGetId(
            array(
                'number'=>$number,
                'type_settlements'=>$type,
                'applicants_id'=>$applicantId,
                'proceeding_states_id'=>14,
                'created_at' =>  \Carbon\Carbon::now()
            )
        );
        return $id;
    }

    public function getProceedings()
    {
        return DB::table('proceedings')
            ->select('proceedings.*')
            ->get();
    }

    public function getProceedingsOrder()
    {
        return DB::table('proceedings')
            ->orderBy('id', 'desc')
            ->select('proceedings.*')
            ->get();
    }

    public function getProceedById($id)
    {
        return DB::table('proceedings')
            ->where([
                ['proceedings.id', '=', $id]
            ])
            ->select('proceedings.*')
            ->get();
    }

    public function getProceedByIdWithDues($id)
    {
        return DB::table('proceedings')
            ->where([
                ['proceedings.id', '=', $id]
            ])
            ->join('dues', 'proceedings.number', '=', 'dues.number')
            ->select('proceedings.id AS proceedingid','dues.*')
            ->get();
    }

    public function getProceedByNumber($number)
    {
        return DB::table('proceedings')
            ->where([
                ['proceedings.number', '=', $number]
            ])
            ->select('proceedings.*')
            ->get();
    }

    
    public function getActionsByProceed($id)
    {
        return DB::table('actions')
            ->where([
                ['actions.proceedings_id', '=', $id]
            ])
            ->select('actions.*')
             ->whereNull('actions.deleted_at')
            ->get();
    }


    public function getProceedByCC($number)
    {
        return DB::table('proceedings')
            ->join('summoneds', 'summoneds.id', '=', 'proceedings.summoneds_id')
            ->where([
                ['summoneds.identification', '=', $number]
            ])
            ->select('proceedings.*')
            ->get();
    }
    
        public function getProceedByApplicantCC($number)
    {
        return DB::table('proceedings')
            ->join('applicants', 'applicants.id', '=', 'proceedings.applicants_id')
            ->where([
                ['applicants.identification', '=', $number]
            ])
            ->select('proceedings.*')
            ->get();
    }

    public function getProceedByName($name)
    {
        return DB::table('proceedings')
            ->join('applicants', 'applicants.id', '=', 'proceedings.applicants_id')
            ->whereRaw("concat(applicants.name, ' ', applicants.lastname) like '%" .$name. "%' ")
            ->select('proceedings.*')
            ->get();
    }

}
