<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="User",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rols_id",
 *          description="rols_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'lastname',
        'phone',
        'rols_id',
        'email',
        'rol',
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'lastname' => 'string',
        'phone' => 'string',
        'rols_id' => 'integer',
        'email' => 'string',
        'rol' => 'integer',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'lastname' => 'required',
        'phone' => 'required',
        'email' => 'required'
    ];


    public function getUserById($id)
    {
        return DB::table('users')
            ->where([
                ['users.id', '=', $id],
            ])
            ->whereNull('users.deleted_at')
            ->select('users.*')
            ->get();
    }
        public function getRolById($id)
    {
        return DB::table('users')
            ->where([
                ['users.id', '=', $id],
            ])
            ->whereNull('users.deleted_at')
            ->select('users.rol')
            ->get();
    }
    

    public function getConciliators()
    {
        return DB::table('users')
            ->where([
                ['users.rol', '=', 'conciliador'],
            ])
            ->whereNull('users.deleted_at')
            ->select('users.*')
            ->get();
    }
    public function getUserByEmail($email)
    {
            return DB::table('users')
                ->where([
                    ['users.email', '=', $email]
                ])
                ->whereNull('users.deleted_at')
                ->select('users.*')
                ->get();
    }

    public function validateEmail($email)
    {
        return DB::table('users')
            ->where([
                ['email', '=', $email]
            ])
            ->select('users.*')
            ->get();
    }


    public function RegisterAdmin($name, $lastname, $email, $password, $phone, $rol = 'admin')
    {
        $id = DB::table('users')->insertGetId(
            array(
                'name' => $name, 'lastname' => $lastname, 'email' => $email,
                'password' => $password, 'phone' => $phone,
                'avatar' => "test.jpg", 'active' => 1,
                'rol'=>$rol,
                'created_at' =>  \Carbon\Carbon::now()
            )
        );
    }


    
}
