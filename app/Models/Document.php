<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="Document",
 *      required={"document_type", "proceedings_id", "extension", "created_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="document_type",
 *          description="document_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="proceedings_id",
 *          description="proceedings_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="extension",
 *          description="extension",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Document extends Model
{
    use SoftDeletes;

    public $table = 'documents';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'document_type',
        'proceedings_id',
        'extension',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'document_type' => 'string',
        'proceedings_id' => 'integer',
        'extension' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'document_type' => 'required',
        'proceedings_id' => 'required',
        'extension' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function proceedings()
    {
        return $this->belongsTo(\App\Models\Proceedings::class, 'proceedings_id');
    }


    public function getById($id)
    {
            return DB::table('documents')
                ->where([
                    ['documents.proceedings_id', '=', $id]
                ])
                ->whereNull('documents.deleted_at')
                ->select('documents.*')
                ->get();
    }
}
