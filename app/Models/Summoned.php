<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
/**
 * @SWG\Definition(
 *      definition="Summoned",
 *      required={"name", "identification", "email", "address", "phone", "created_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="identification",
 *          description="identification",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Summoned extends Model
{
    use SoftDeletes;

    public $table = 'summoneds';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'identification',
        'lastname',
        'email',
        'address',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'lastname' => 'string',
        'identification' => 'string',
        'email' => 'string',
        'address' => 'string',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'identification' => 'required',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function proceedings()
    {
        return $this->hasMany(\App\Models\Proceedings::class, 'summoneds_id');
    }


    public function getById($id)
    {
            return DB::table('summoneds')
                ->where([
                    ['summoneds.id', '=', $id]
                ])
                ->whereNull('summoneds.deleted_at')
                ->select('summoneds.*')
                ->get();
    }

    public function get()
    {
            return DB::table('summoneds')
            ->where([
                    ['summoneds.name', '!=', '']
                ])
                ->whereNull('summoneds.deleted_at')
                ->select('summoneds.*')
                ->orderBy('name', 'ASC')
                ->get();
    }
}
