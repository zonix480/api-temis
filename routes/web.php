<?php
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('send-mail', [MailController::class, 'index']);

Route::resource('analytics', 'AnalyticsController');

Route::resource('users', 'UserController');

Route::resource('analytics', 'AnalyticsController');


Route::resource('proceedings', 'ProceedingController');

Route::resource('applicants', 'ApplicantController');

Route::resource('summoneds', 'SummonedController');

Route::resource('proceedingStates', 'ProceedingStateController');

Route::resource('actions', 'ActionController');

Route::resource('actionTypes', 'ActionTypesController');

Route::resource('dues', 'DuesController');

Route::resource('purses', 'PurseController');

Route::resource('documents', 'DocumentController');