<?php

use Illuminate\Http\Request;
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, Accept,charset,boundary,Content-Length');
header('Access-Control-Allow-Origin: *');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('v1/users', 'UserAPIController');

Route::resource('v1/categories', 'CategoryAPIController');

Route::resource('v1/configurations', 'ConfigurationAPIController');

Route::resource('v1/designs', 'DesignAPIController');
Route::post('v1/loginuser', 'UserAPIController@loginuser');
Route::get('v1/expediente/{number}/{type}', 'ProceedingAPIController@expediente');


Route::resource('v1/layers', 'LayerAPIController');

Route::resource('v1/proceedings', 'ProceedingAPIController');

Route::resource('v1/applicants', 'ApplicantAPIController');

Route::resource('v1/summoneds', 'SummonedAPIController');

Route::resource('v1/proceeding_states', 'ProceedingStateAPIController');

Route::resource('v1/actions', 'ActionAPIController');

Route::resource('v1/action_types', 'ActionTypesAPIController');

Route::resource('v1/dues', 'DuesAPIController');

Route::resource('v1/purses', 'PurseAPIController');

Route::resource('v1/documents', 'DocumentAPIController');
Route::get('v1/cartera/{id}', 'ProceedingAPIController@cartera');
