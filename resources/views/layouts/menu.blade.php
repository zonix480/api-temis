@if(Session::get('currentRol') == 'admin')
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-user"></i><span>Usuarios</span></a>
</li>
<li class="{{ Request::is('applicants*') ? 'active' : '' }}">
    <a href="{{ route('applicants.index') }}"><i class="fa fa-user"></i><span>Solicitantes</span></a>
</li>

<li class="{{ Request::is('summoneds*') ? 'active' : '' }}">
    <a href="{{ route('summoneds.index') }}"><i class="fa fa-user"></i><span>Convocados</span></a>
</li>
<li class="{{ Request::is('proceedings*') ? 'active' : '' }}">
    <a href="{{ route('proceedings.index') }}"><i class="fa fa-address-book-o"></i><span>Expedientes</span></a>
</li>
<li class="{{ Request::is('actions*') ? 'active' : '' }}">
    <a href="{{ route('actions.index') }}"><i class="fa fa-eye"></i><span>Actuaciones</span></a>
</li>
<li class="{{ Request::is('documents*') ? 'active' : '' }}">
    <a href="{{ route('documents.index') }}"><i class="fa fa-file-archive-o"></i><span>Documentos</span></a>
</li>

<li class="{{ Request::is('purses*') ? 'active' : '' }}">
    <a href="{{ route('purses.index') }}"><i class="fa fa-money"></i><span>Carteras</span></a>
</li>

<li class="{{ Request::is('dues*') ? 'active' : '' }}">
    <a href="{{ route('dues.index') }}"><i class="fa fa-usd"></i><span>Recibos </span></a>
</li>

<li class="{{ Request::is('proceedingStates*') ? 'active' : '' }}">
    <a href="{{ route('proceedingStates.index') }}"><i class="fa fa-exchange"></i><span>Estados de procedimientos</span></a>
</li>


<li class="{{ Request::is('actionTypes*') ? 'active' : '' }}">
    <a href="{{ route('actionTypes.index') }}"><i class="fa fa-files-o"></i><span>Tipos de actuaciones</span></a>
</li>

<li class="">
    <a href="{{ route('analytics.index') }}"><i class="fa fa-area-chart"></i><span>Analiticas</span></a>
</li>



@endif

@if(Session::get('currentRol') == 'conciliador')

<li class="{{ Request::is('actions*') ? 'active' : '' }}">
    <a href="{{ route('actions.index') }}"><i class="fa fa-eye"></i><span>Actuaciones</span></a>
</li>
<li class="{{ Request::is('documents*') ? 'active' : '' }}">
    <a href="{{ route('documents.index') }}"><i class="fa fa-file-archive-o"></i><span>Documentos</span></a>
</li>
@endif
<li class="{{ Request::is('actionTypes*') ? 'active' : '' }}">
<a href="{!! url('/logout') !!}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>
</li>
<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

