<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombres:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Apellidos:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telefono:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Rols Id Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('rols_id', 'Rols Id:') !!}
    {!! Form::number('rols_id', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Rols Id Field -->
@if(isset($rol))
    @if($rol[0]->rol == "conciliador")
<div class="form-group col-sm-6">
    {!! Form::label('rol', 'Conciliador:') !!}
     {!! Form::checkbox('rol', true, ['class' => 'form-control']) !!}
    
</div> 
@else
<div class="form-group col-sm-6">
    {!! Form::label('rol', 'Conciliador:') !!}
    {!! Form::checkbox('rol', false, ['class' => 'form-control']) !!}
</div> 
@endif
@else
<div class="form-group col-sm-6">
    {!! Form::label('rol', 'Conciliador:') !!}
    {!! Form::checkbox('rol', null, ['class' => 'form-control']) !!}
</div> 
@endif

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Contraseña:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Remember Token Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-default">Cancelar</a>
</div>
