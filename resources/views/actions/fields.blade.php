<!-- Date Field -->
@if(!empty($action->date))
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Fecha:') !!}
    {!! Form::date('date', $action->date, ['class' => 'form-control','id'=>'date']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Fecha:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>
@endif

@push('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush



@if(!empty($action->action_type))
<div class="form-group col-sm-6">
    {!! Form::label('action_type', 'Estado :') !!}
    <select class="form-control"  name="action_">
        @if(count($procedingSelected) != 0)
        <option value="{{$procedingSelected[0]->id}}">{{$procedingSelected[0]->number}}</option>
        @endif
        <?php foreach ($actionTypes as $process) { ?>
            <option value="<?= $process->action_type?>">{{$process->action_type}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('action_type', 'Acción:') !!}
    <select class="form-control" name="action_">
    <?php foreach ($actionTypes as $process) { ?>
              <option value="<?= $process->action_type?>">{{$process->action_type}}</option>
           <?php
        } ?>
    </select>
</div>
@endif

<!-- Observation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observation', 'Observación:') !!}
    {!! Form::textarea('observation', null, ['class' => 'form-control']) !!}
</div>

@if(!empty($action->date_start))
<!-- Date Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_start', 'Fecha inicio Termino:') !!}
    {!! Form::date('date_start', $action->date_start, ['class' => 'form-control','id'=>'date_start']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('date_start', 'Fecha inicio Termino:') !!}
    {!! Form::date('date_start', null, ['class' => 'form-control','id'=>'date_start']) !!}
</div>
@endif


@push('scripts')
    <script type="text/javascript">
        $('#date_start').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush
@if(!empty($action->date_end))
<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', 'Fecha Fin Termino:') !!}
    {!! Form::date('date_end', $action->date_end, ['class' => 'form-control','id'=>'date_end']) !!}
</div>
@else
<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', 'Fecha Fin Termino:') !!}
    {!! Form::date('date_end', null, ['class' => 'form-control','id'=>'date_end']) !!}
</div>
@endif

@push('scripts')
    <script type="text/javascript">
        $('#date_end').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush
@if(!empty($action->proceedings_id))
<div class="form-group col-sm-6">
    {!! Form::label('proceedings_id', 'Expediente :') !!}
    <select class="form-control"  name="proceedings_id">
         @if(count($procedingnewSelected) != 0)
        <option value="{{$procedingnewSelected[0]->id}}">{{$procedingnewSelected[0]->number}}</option>
        @endif
        <?php foreach ($proceedings as $process) { ?>
              <option value="<?= $process->id?>">{{$process->number}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('proceedings_id', 'Expediente:') !!}
    <select class="form-control" name="proceedings_id">
    <?php foreach ($proceedings as $process) { ?>
              <option value="<?= $process->id?>">{{$process->number}}</option>
           <?php
        } ?>
    </select>
</div>
@endif




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('actions.index') }}" class="btn btn-default">Cancelar</a>
</div>
