<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombres:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Apellidos:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Identification Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identification', 'Identificacion:') !!}
    {!! Form::number('identification', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Direccion:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telefono:') !!}
    {!! Form::number('phone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <h2>Expediente</h2>
</div>

<!-- Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number', 'Numero de expediente:') !!}
    {!! Form::text('number', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Fecha de primer pago:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>
<!-- Reconciliation Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total Tarifa Centro de Conciliacion:') !!}
    <input  name="total" class="form-control" data-type="currency" >
</div>
<!-- Valor cuota -->
<div class="form-group col-sm-6">
    {!! Form::label('reconciliation_fee', 'Valor Cuota:') !!}
    <input  name="reconciliation_fee" class="form-control" data-type="currency" >
</div>

<?php
    $quotas;
    for ($i=1; $i <=8 ; $i++) { 
        # code...
        $quotas[$i] = $i;
    }
?>
<div class="form-group col-sm-6">
    {!! Form::label('quotas', 'Numero de cuotas:') !!}
    <select class="form-control" name="quotas">
    <?php foreach ($quotas as $quota) { ?>
              <option value="<?= $quota?>">{{$quota}}</option>
           <?php
        } ?>
    </select>
</div>

<!-- Type Settlements Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_settlements', 'Tipo de Conciliación:') !!}
    <select class="form-control" name="type_settlements">
              <option value="INSOLVENCIA">INSOLVENCIA</option>
              <option value="EXTRA PROCESAL">EXTRA PROCESAL</option>
              <option value="EXTRA PROCESAL">ARBITRAMIENTO MINIMO</option>
              <option value="EXTRA PROCESAL">ARBITRAMIENTO MENOR</option>
              <option value="EXTRA PROCESAL">ARBITRAMIENTO MAYOR</option>
    </select>
</div>

<!-- ASESOR -->
<div class="form-group col-sm-6">
    {!! Form::label('asesor', 'Nombre Asesor:') !!}
    <input required  name="asesor" class="form-control" >
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('applicants.index') }}" class="btn btn-default">Cancelar</a>
</div>

<script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
<script>// Jquery Dependency

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "$" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "$" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}


</script>
