<!-- Document Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('document_type', 'Tipo de documento (Ejemplo: Constancia de suspensión):') !!}
    {!! Form::text('document_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Proceedings Id Field -->
@if(!empty($document->proceedings_id))
<div class="form-group col-sm-6">
    {!! Form::label('proceedings_id', 'Expediente :') !!}
    <select class="form-control"  name="proceedings_id">
        <option value="{{$procedingSelected[0]->id}}">{{$procedingSelected[0]->number}}</option>
        <?php foreach ($proceedings as $process) { ?>
              <option value="<?= $process->id?>">{{$process->number}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('proceedings_id', 'Expediente:') !!}
    <select class="form-control" name="proceedings_id">
    <?php foreach ($proceedings as $process) { ?>
              <option value="<?= $process->id?>">{{$process->number}}</option>
           <?php
        } ?>
    </select>
</div>
@endif

<!-- Extension Field -->
<div class="form-group col-sm-6">
    {!! Form::label('extension', 'Extension:') !!}
    <select class="form-control"  name="extension">
        <option value="pdf">PDF</option>
    </select>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Archivo:') !!}
    {!! Form::file('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('documents.index') }}" class="btn btn-default">Cancelar</a>
</div>
