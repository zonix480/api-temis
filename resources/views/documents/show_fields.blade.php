<!-- Document Type Field -->
<div class="form-group">
    {!! Form::label('document_type', 'Document Type:') !!}
    <p>{{ $document->document_type }}</p>
</div>

<!-- Proceedings Id Field -->
<div class="form-group">
    {!! Form::label('proceedings_id', 'Proceedings Id:') !!}
    <p>{{ $document->proceedings_id }}</p>
</div>

<!-- Extension Field -->
<div class="form-group">
    {!! Form::label('extension', 'Extension:') !!}
    <p>{{ $document->extension }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $document->name }}</p>
</div>

