<!-- Passive Capital Field -->
<div class="form-group">
    {!! Form::label('passive_capital', 'Passive Capital:') !!}
    <p>{{ $purse->passive_capital }}</p>
</div>

<!-- Reconciliation Fee Field -->
<div class="form-group">
    {!! Form::label('reconciliation_fee', 'Reconciliation Fee:') !!}
    <p>{{ $purse->reconciliation_fee }}</p>
</div>

<!-- Asesorname Field -->
<div class="form-group">
    {!! Form::label('asesorname', 'Asesorname:') !!}
    <p>{{ $purse->asesorname }}</p>
</div>

<!-- Proceedings Id Field -->
<div class="form-group">
    {!! Form::label('proceedings_id', 'Proceedings Id:') !!}
    <p>{{ $purse->proceedings_id }}</p>
</div>

<!-- Pursecol Field -->
<div class="form-group">
    {!! Form::label('pursecol', 'Pursecol:') !!}
    <p>{{ $purse->pursecol }}</p>
</div>

