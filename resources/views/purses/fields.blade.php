<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php //print_r($procedingSelected)?>
<!-- Passive Capital Field -->
<div class="form-group col-sm-6">
  {!! Form::label('passive_capital', 'Capital pasivo:') !!}
  @if(!$purse->passive_capital)
  <input name="passive_capital" class="form-control" data-type="currency">
  @else
  {!! Form::number('passive_capital', null, ['class' => 'form-control']) !!}
  @endif
</div>

<!-- Reconciliation Fee Field -->
<div class="form-group col-sm-6">
  {!! Form::label('reconciliation_fee', 'Tarifa de centro de reconciliación:') !!}
  @if(!$purse->reconciliation_fee)
  <input name="reconciliation_fee" class="form-control" data-type="currency">
  @else
  {!! Form::number('reconciliation_fee', null, ['class' => 'form-control']) !!}
  @endif

</div>

<!-- Asesorname Field -->
<div class="form-group col-sm-6">
  {!! Form::label('asesorname', 'Nombre del asesor:') !!}
  {!! Form::text('asesorname', null, ['class' => 'form-control']) !!}
</div>
@if(!empty($purse->proceedings_id))
<div class="form-group col-sm-6">
  {!! Form::label('proceedings_id', 'Expediente :') !!}
  <select disaled class="form-control" name="proceedings_id">
    <option value="{{$procedingSelected[0]->proceedingid}}">{{$procedingSelected[0]->number}}</option>
    <?php foreach ($proceedings as $process) { ?>
      <option value="<?= $process->id ?>">{{$process->number}}</option>
    <?php
    } ?>
  </select>
</div>
@else
<div class="form-group col-sm-6">
  {!! Form::label('proceedings_id', 'Expediente:') !!}
  <select class="form-control" name="proceedings_id">
    <?php foreach ($proceedings as $process) { ?>
      <option value="<?= $process->id ?>">{{$process->number}}</option>
    <?php
    } ?>
  </select>
</div>
@endif
<!--

<?php
$days;
for ($i = 1; $i <= 31; $i++) {
  # code...
  $days[$i] = $i;
}
?>
<div class="form-group col-sm-6">
  {!! Form::label('pursecol', 'Dia de corte:') !!}
  <select class="form-control" name="pursecol">
    <?php foreach ($days as $day) { ?>
      <option value="<?= $day ?>">{{$day}}</option>
    <?php
    } ?>
  </select>
</div>-->

<?php
$QuotasSelected = $purse->quotas;
$quotas;
for ($i = 1; $i <= 12; $i++) {
  # code...
  $quotas[$i] = $i;
}
?>
<div class="form-group col-sm-6">
  {!! Form::label('quotas', 'Número de cuotas:') !!}
  <select id="quotasselected" class="form-control" name="quotas">
  <option selected value="<?=$purse->quotas?>"><?=$purse->quotas?></option>
    <?php foreach ($quotas as $quota) { ?>
      <option value="<?= $quota ?>">{{$quota}}</option>
    <?php
    } ?>
  </select>
</div>


<!-- Asesorname Field
<div class="form-group col-sm-6">
  {!! Form::label('firstDate', 'Fecha primer pago:') !!}
  {!! Form::date('firstDate', null, ['class' => 'form-control','required' => 'required']) !!}
</div> -->
<div class="container col-sm-12" id="quotasform">
  <div class="form-group">
    <h2>Cuotas</h2>
  </div>
 
  <!-- CUOTAS ACA -->
  @foreach($procedingSelected as $dues)
  <div class="form-group col-sm-12" class="quota" id="quota{{$loop->index+1}}">
   
    <h1 class="titulo">Cuota {{$loop->index+1}} </h1>
    <!-- Due Number Field -->
    @if(!empty($dues->due_number))
    <div class="form-group col-sm-6">
      {!! Form::label('due_number', 'Numero de cuota:') !!}
      <input class="form-control" type="text" name="due_number_{{$loop->index+1}}" readonly value="{{$dues->due_number}}">
    </div>
    @else
    <div class="form-group col-sm-6">
      {!! Form::label('due_number', 'Numero de cuota:') !!}
      {!! Form::text('due_number', null, ['class' => 'form-control']) !!}
    </div>
    @endif

    @if(!empty($dues->due_value))
    <!-- Due Value Field -->
    <div class="form-group col-sm-6">
      {!! Form::label('due_value', 'Valor de cuota:') !!}
      <input class="form-control" type="text" name="due_value_{{$loop->index+1}}"  value="{{$dues->due_value}}">
    </div>
    @else
    <div class="form-group col-sm-6">
      {!! Form::label('due_value', 'Valor de cuota:') !!}
      {!! Form::text('due_value', null, ['class' => 'form-control']) !!}
    </div>
    @endif
    @if(!empty($dues->date))
    <!-- Date Field -->
    <div class="form-group col-sm-6">
      {!! Form::label('date', 'Fecha oportuna de pago:') !!}
      <input class="form-control" type="date" name="date_{{$loop->index+1}}"  value="{{$dues->date}}">
      
    </div>
    @else
    <!-- Date Field -->
    <div class="form-group col-sm-6">
      {!! Form::label('date', 'Fecha:') !!}
      {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
    </div>
    @endif


    @push('scripts')
    <script type="text/javascript">
      $('#date').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        useCurrent: false
      });
     
    </script>
    @endpush

    <!-- Observation Field -->
    <div class="form-group col-sm-12 col-lg-12">
      {!! Form::label('observation', 'Observacion:') !!}
      <textarea class="form-control" name="observation_{{$loop->index+1}}"  >{{$dues->observation}}</textarea>
    </div>

    <!-- State Field -->
    <!-- <div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::text('state', null, ['class' => 'form-control']) !!}
</div> -->

    <div class="form-group col-sm-6">
      {!! Form::label('payment_type', 'Tipo de pago:') !!}
      <select class="form-control" name="payment_type_{{$loop->index+1}}">
        <option value="Efectivo">Efectivo</option>
        <option value="Consignacion">Consignacion</option>
        <option value="PSE">PSE</option>
        <option value="Tarjeta de credito">Tarjeta de credito</option>

      </select>
    </div>

    <?php
    $states = ["No pago", "Pagado"];
    ?>
    @if(!empty($dues->state))
    <div class="form-group col-sm-6">
      {!! Form::label('purse_id', 'Estado :') !!}
      <select class="form-control" name="state_{{$loop->index+1}}">
        <option value="{{$dues->state}}">{{$dues->state}}</option>
        <?php foreach ($states as $state) { ?>
          <option value="<?= $state ?>">{{$state}}</option>
        <?php
        } ?>
      </select>
    </div>
    @else
    <div class="form-group col-sm-6">
      {!! Form::label('proceedings_id', 'Cartera:') !!}
      <select class="form-control" name="state_{{$loop->index+1}}">
        <option value="{{$dues->state}}">{{$dues->state}}</option>
        <?php foreach ($states as $state) { ?>
          <option value="<?= $state ?>">{{$state}}</option>
        <?php
        } ?>
      </select>
    </div>
    @endif
  </div>

  @endforeach
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
  {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
  <a href="{{ route('purses.index') }}" class="btn btn-default">Cancelar</a>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
  // Jquery Dependency

  $("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() {
      formatCurrency($(this), "blur");
    }
  });


  function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }


  function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") {
      return;
    }

    // original length
    var original_len = input_val.length;

    // initial caret position 
    var caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {

      // get position of first decimal
      // this prevents multiple decimals from
      // being entered
      var decimal_pos = input_val.indexOf(".");

      // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);

      // add commas to left side of number
      left_side = formatNumber(left_side);

      // validate right side
      right_side = formatNumber(right_side);

      // On blur make sure 2 numbers after decimal
      if (blur === "blur") {
        right_side += "00";
      }

      // Limit decimal to only 2 digits
      right_side = right_side.substring(0, 2);

      // join number by .
      input_val = "$" + left_side + "." + right_side;

    } else {
      // no decimal entered
      // add commas to number
      // remove all non-digits
      input_val = formatNumber(input_val);
      input_val = "$" + input_val;

      // final formatting
      if (blur === "blur") {
        input_val += ".00";
      }
    }

    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
  }
  $( document ).ready(function() {
    $("#quotasselected").change(function(){
      let latestExist = 0;
      for (let count = 1; count <= 12; count++) {
        if ($("#quota"+count).length) {
          latestExist = latestExist + 1;
        }
      }
      for (let con = parseInt($(this).val())+1; con <= 12; con++) {
        if ($("#quota"+con).length) {
          console.log("#quota"+con+" ya existe");
          $("#quota"+con).remove();
        }
      }
      console.log("empiece a agregar desde la quota "+(latestExist+1)+" hasta la "+$(this).val());
      if(latestExist < $(this).val()){
        for (let index = latestExist+1; index <= $(this).val(); index++) {
          console.log("SE CREA EL "+index);
          var div = document.getElementById('quota1');
          clone = div.cloneNode(true); // true means clone all childNodes and all event handlers
          console.log(clone.children);
          clone.id = "quota"+index;
          clone.children[0].innerHTML = "Cuota "+index;
          clone.children[1].children[1].value = index;
          clone.children[1].children[1].name = "due_number_"+index;
          clone.children[2].children[1].value = "";
          clone.children[2].children[1].name = "due_value_"+index;
          clone.children[3].children[1].value = "";
          clone.children[3].children[1].name = "date_"+index;
          clone.children[4].children[1].value = "";
          clone.children[4].children[1].name = "observation_"+index;
          clone.children[5].children[1].name = "payment_type_"+index;
          clone.children[6].children[1].name = "state_"+index;

          document.getElementById("quotasform").appendChild(clone);
        }
      }
 
    });
});
  

</script>