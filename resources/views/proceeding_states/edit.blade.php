@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Estado de Expediente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($proceedingState, ['route' => ['proceedingStates.update', $proceedingState->id], 'method' => 'patch']) !!}

                        @include('proceeding_states.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection