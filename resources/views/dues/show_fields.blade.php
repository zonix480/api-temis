<!-- Due Number Field -->
<div class="form-group">
    {!! Form::label('due_number', 'Due Number:') !!}
    <p>{{ $dues->due_number }}</p>
</div>

<!-- Due Value Field -->
<div class="form-group">
    {!! Form::label('due_value', 'Due Value:') !!}
    <p>{{ $dues->due_value }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $dues->date }}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{{ $dues->observation }}</p>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{{ $dues->state }}</p>
</div>

<!-- Purse Id Field -->
<div class="form-group">
    {!! Form::label('purse_id', 'Purse Id:') !!}
    <p>{{ $dues->purse_id }}</p>
</div>

