<!-- Due Number Field -->
@if(!empty($dues->due_number))
<div class="form-group col-sm-6">
    {!! Form::label('due_number', 'Numero de cuota:') !!}
    {!! Form::text('due_number', null, ['class' => 'form-control','readonly' => 'true']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('due_number', 'Numero de cuota:') !!}
    {!! Form::text('due_number', null, ['class' => 'form-control']) !!}
</div>
@endif

@if(!empty($dues->due_value))
<!-- Due Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('due_value', 'Valor de cuota:') !!}
    {!! Form::text('due_value', null, ['class' => 'form-control','readonly' => 'true']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('due_value', 'Valor de cuota:') !!}
    {!! Form::text('due_value', null, ['class' => 'form-control']) !!}
</div>
@endif
@if(!empty($dues->date))
<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Fecha oportuna de pago:') !!}
    {!! Form::date('date',$dues->date, ['class' => 'form-control','id'=>'date','readonly' => 'true']) !!}
</div>
@else
<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Fecha:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>
@endif


@push('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Observation Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observation', 'Observacion:') !!}
    {!! Form::textarea('observation', null, ['class' => 'form-control']) !!}
</div>

<!-- State Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::text('state', null, ['class' => 'form-control']) !!}
</div> -->

<div class="form-group col-sm-6">
    {!! Form::label('payment_type', 'Tipo de pago:') !!}
    <select class="form-control"  name="payment_type" >
        <option value="Efectivo">Efectivo</option>
        <option value="Consignacion">Consignacion</option>
        <option value="PSE">PSE</option>
        <option value="Tarjeta de credito">Tarjeta de credito</option>
        
    </select>
</div>

<?php
$states = ["No pago","Pagado"];
?>
@if(!empty($dues->state))
<div class="form-group col-sm-6">
    {!! Form::label('purse_id', 'Estado :') !!}
    <select class="form-control"  name="state" >
        <option value="{{$dues->state}}">{{$dues->state}}</option>
        <?php foreach ($states as $state) { ?>
              <option value="<?= $state?>">{{$state}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('proceedings_id', 'Cartera:') !!}
    <select class="form-control"  name="state" >
        <option value="{{$dues->state}}">{{$dues->state}}</option>
        <?php foreach ($states as $state) { ?>
              <option value="<?= $state?>">{{$state}}</option>
           <?php
        } ?>
    </select>
</div>
@endif

@if(!empty($dues->purse_id))
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('purse_id', 'Cartera:') !!}
    <select  class="form-control"   name="purse_id" >
        <option selected value="{{$purseSelected[0]->id}}">{{$purseSelected[0]->id}}</option>
        <?php foreach ($purses as $process) { ?>
              <option value="<?= $process->id?>">{{$process->id}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('proceedings_id', 'Cartera:') !!}
    <select class="form-control" name="purse_id">
    <?php foreach ($purses as $process) { ?>
              <option value="<?= $process->id?>">{{$process->id}}</option>
           <?php
        } ?>
    </select>
</div>
@endif
@if(!empty($dues->payment_date))
<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_date', 'Fecha en la que pago:') !!}
    {!! Form::date('payment_date', $dues->payment_date, ['class' => 'form-control','id'=>'payment_date']) !!}
</div>
@else
<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_date', 'Fecha en la que pago:') !!}
    {!! Form::date('payment_date', null, ['class' => 'form-control','id'=>'payment_date']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('dues.index') }}" class="btn btn-default">Cancelar</a>
</div>
