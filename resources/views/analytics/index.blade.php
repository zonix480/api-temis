@extends('layouts.app')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<!-- CSS only -->
<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous"> -->
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
<style>
    .px-15{
        padding: 20px 15px;
    }
    p-0{
        padding: 0;
    }
    .analytics-text{
        font-size: 15px;
        letter-spacing: 3px;
        text-transform: uppercase;
        margin: 0;
        font-weight: bold;
    }
    .analytics-text-number{
        font-size: 40px;
    }
</style>
@section('content')
    <section class="content-header">
        <h1>Analiticas</h1>
    </section>
    <br>
    <div class="content" style="display:grid; grid-template-columns: 1fr 1fr 1fr 1fr; column-gap: 30px; min-height: auto;">
        <!-- FACTURAS SIN PAGAR -->
        <div class="px-15 box box-primary">
            <h1 class="analytics-text">Facturas Sin pagar</h1>
            <hr>
            <span class="analytics-text-number"><?php echo count($duesforpayment); ?></span>
        </div>
        <!-- DINERO POR PAGAR -->
        <div class="px-15 box box-primary">
            <h1 class="analytics-text">Dinero por pagar</h1>
            <hr>
            <span class="analytics-text-number">
                <?php 
                    $value = 0;
                    foreach ($duesforpayment as $duesvalue) {
                        $value = $value + $duesvalue->due_value;
                    }
                    echo "$ ".number_format($value, 2);
                ?>
            </span>
        </div>
        <!-- DINERO PAGADO -->
        <div class="px-15 box box-primary">
            <h1 class="analytics-text">Dinero Pagado ULTIMO MES <i class="fas fa-dollar-sing"></i></h1>
            <hr>
            <span class="analytics-text-number">
                <?php 
                    $value = 0;
                    foreach ($duespaylastmonth as $duespayvalue) {
                        $value = $value + $duespayvalue->due_value;
                    }
                    echo "$ ".number_format($value, 2);
                ?>
            </span>
        </div>
    </div>

    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="center ml-4" style="margin-left:20px">
                <h2>Numero de facturas por mes</h2>
            </div>
            <div class="box-body">
            <canvas id="myChart" style="height:40vh; width:80vw"></canvas>
            <?php 
                $enero = 0;
                $febrero = 0;
                $marzo = 0;
                $abril = 0;
                $mayo = 0;
                $junio = 0;
                $julio = 0;
                $agosto = 0;
                $septiembre = 0;
                $octubre = 0;
                $noviembre = 0;
                $diciembre = 0;
                foreach ($dues as $due) {
                    $mes = substr($due->payment_date, 5,2); 
                    switch ($mes) {
                        case '01':
                            $enero = $enero +1;
                            break;
                        case '02':
                            $febrero = $febrero +1;
                            break;
                        case '03':
                            $marzo = $marzo +1;
                            break;
                        case '04':
                            $abril = $abril +1;
                            break;
                        case '05':
                            $mayo = $mayo +1;
                            break;
                        case '06':
                            $junio = $junio +1;
                            break;
                        case '07':
                            $julio = $julio +1;
                            break;
                        case '08':
                            $agosto = $agosto +1;
                            break; 
                        case '09':
                            $septiembre = $septiembre +1;
                            break;
                        case '10':
                            $octubre = $octubre +1;
                            break;
                        case '11':
                            $noviembre = $noviembre +1;
                            break;
                        case '12':
                            $diciembre = $diciembre +1;
                            break; 
                        default:
                            # code...
                            break;
                    }
                    
                }
            ?>
<script>
const ctx = document.getElementById('myChart');

const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Enero', 'Febero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        datasets: [{
            label: '# de Pagos',
            data: [<?=$enero?>,<?=$febrero?>,<?=$marzo?>,<?=$abril?>,<?=$mayo?>,<?=$junio?>,<?=$julio?>,<?=$agosto?>,<?=$septiembre?>,<?=$octubre?>,<?=$noviembre?>,<?=$diciembre?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection


