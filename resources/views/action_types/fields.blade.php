<!-- Action Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Tipo de actuacion', 'Tipo de actuacion:') !!}
    {!! Form::text('action_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('actionTypes.index') }}" class="btn btn-default">Cancelar</a>
</div>
