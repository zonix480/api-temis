@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Action Types
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($actionTypes, ['route' => ['actionTypes.update', $actionTypes->id], 'method' => 'patch']) !!}

                        @include('action_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection