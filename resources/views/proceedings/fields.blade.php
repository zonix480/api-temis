<!-- Application Filing Date Field -->
@if(!empty($proceeding->number))
<div class="form-group col-sm-6">
    {!! Form::label('number', 'Numero de expediente:') !!}
    {!! Form::number('number', $proceeding->number, ['class' => 'form-control','id'=>'number', 'disabled']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('number', 'Numero de expediente:') !!}
    {!! Form::number('number', null, ['class' => 'form-control','id'=>'number']) !!}
</div>
@endif

<!-- Application Filing Date Field -->
@if(!empty($proceeding->application_filing_date))
<div class="form-group col-sm-6">
    {!! Form::label('application_filing_date', 'Fecha de radicación de la solicitud:') !!}
    {!! Form::date('application_filing_date', $proceeding->application_filing_date, ['class' => 'form-control','id'=>'application_filing_date']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('application_filing_date', 'Fecha de radicación de la solicitud:') !!}
    {!! Form::date('application_filing_date', null, ['class' => 'form-control','id'=>'application_filing_date']) !!}
</div>
@endif

@push('scripts')
    <script type="text/javascript">
        $('#application_filing_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Application Filing Date Field -->
@if(!empty($proceeding->date_acceptance_conciliator))
<div class="form-group col-sm-6">
    {!! Form::label('date_acceptance_conciliator', 'Fecha de aceptación - conciliador:') !!}
    {!! Form::date('date_acceptance_conciliator', $proceeding->date_acceptance_conciliator, ['class' => 'form-control','id'=>'date_acceptance_conciliator']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('date_acceptance_conciliator', 'Fecha de aceptación - conciliador:') !!}
    {!! Form::date('date_acceptance_conciliator', null, ['class' => 'form-control','id'=>'date_acceptance_conciliator']) !!}
</div>
@endif

@push('scripts')
    <script type="text/javascript">
        $('#date_acceptance_conciliator').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush
@if(!empty($proceeding->date_acceptance_application))
<!-- Date Acceptance Application Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_acceptance_application', 'Fecha de aceptación de la solicitud:') !!}
    {!! Form::date('date_acceptance_application', $proceeding->date_acceptance_application, ['class' => 'form-control','id'=>'date_acceptance_application']) !!}
</div>
@else
<!-- Date Acceptance Application Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_acceptance_application', 'Fecha de aceptación de la solicitud:') !!}
    {!! Form::date('date_acceptance_application', null, ['class' => 'form-control','id'=>'date_acceptance_application']) !!}
</div>
@endif
@push('scripts')
    <script type="text/javascript">
        $('#date_acceptance_application').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Type Settlements Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_settlements', 'Tipo de Conciliación:') !!}
    <select class="form-control" name="type_settlements">
              <option value="INSOLVENCIA">INSOLVENCIA</option>
              <option value="EXTRA PROCESAL">EXTRA PROCESAL</option>
              <option value="EXTRA PROCESAL">ARBITRAMIENTO MINIMO</option>
              <option value="EXTRA PROCESAL">ARBITRAMIENTO MENOR</option>
              <option value="EXTRA PROCESAL">ARBITRAMIENTO MAYOR</option>
    </select>
</div>

<!-- Arbitrament Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('arbitrament', 'Arbitramiento:') !!}
    <input type="text" value="">
    <select class="form-control" name="arbitrament">
              <option value="MINIMA">MINIMA</option>
              <option value="MENOR">MENOR</option>
              <option value="MAYOR">MAYOR</option>
    </select>
</div> -->
@if(!empty($proceeding->applicants_id))
<div class="form-group col-sm-6">
    {!! Form::label('applicants_id', 'Solicitante :') !!}
    <select disabled class="form-control" name="applicants_id">
        @if(count($applicantSelected) != 0)
        <option value="{{$applicantSelected[0]->id}}">{{$applicantSelected[0]->name}} {{$applicantSelected[0]->lastname}}</option>
        @endif
        <?php foreach ($applicants as $applicant) { ?>
              <option value="<?= $applicant->id?>">{{$applicant->name}} {{$applicant->lastname}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('applicants_id', 'Solicitante:') !!}
    <select disabled class="form-control" name="applicants_id">
    <?php foreach ($applicants as $applicant) { ?>
              <option value="<?= $applicant->id?>">{{$applicant->name}} {{$applicant->lastname}}</option>
           <?php
        } ?>
    </select>
</div>
@endif

@if(!empty($proceeding->summoneds_id))
<div class="form-group col-sm-6">
    {!! Form::label('summoneds_id', 'Convocado :') !!}
    <select class="form-control" name="summoneds_id">
         @if(count($summonedSelected) != 0)
        <option value="{{$summonedSelected[0]->id}}">{{$summonedSelected[0]->name}}</option>
         @endif
        <?php foreach ($summoneds as $applicant) { ?>
              <option value="<?= $applicant->id?>">{{$applicant->name}} {{$applicant->lastname}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('summoneds_id', 'Convocado:') !!}
    <select class="form-control" name="summoneds_id">
         <option value="">Seleccione</option>
    <?php foreach ($summoneds as $applicant) { ?>
       
              <option value="<?= $applicant->id?>">{{$applicant->name}} {{$applicant->lastname}}</option>
           <?php
        } ?>
    </select>
</div>
@endif


@if(!empty($proceeding->proceeding_states_id))
<div class="form-group col-sm-6">
    {!! Form::label('proceeding_states_id', 'Estado :') !!}
    <select class="form-control"  name="proceeding_states_id">
         @if(count($stateSelected) != 0)
        <option value="{{$stateSelected[0]->id}}">{{$stateSelected[0]->name}}</option>
        @endif
        <?php foreach ($states as $applicant) { ?>
              <option value="<?= $applicant->id?>">{{$applicant->name}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('proceeding_states_id', 'Estado:') !!}
    <select class="form-control" name="proceeding_states_id">
    <?php foreach ($states as $applicant) { ?>
              <option value="<?= $applicant->id?>">{{$applicant->name}}</option>
           <?php
        } ?>
    </select>
</div>
@endif


@if(!empty($proceeding->conciliator_id))
<div class="form-group col-sm-6">
    {!! Form::label('proceeding_states_id', 'Conciliador :') !!}
    <select class="form-control"  name="conciliator_id">
        <option value="{{$conciliatorSelected[0]->id}}">{{$conciliatorSelected[0]->name}} {{$conciliatorSelected[0]->lastname}}</option>
        <?php foreach ($conciliators as $conciliator) { ?>
              <option value="<?= $conciliator->id?>">{{$conciliator->name}} {{$conciliator->lastname}}</option>
           <?php
        } ?>
    </select>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('conciliator_id', 'Conciliador:') !!}
    <select class="form-control" name="conciliator_id">
        <option>Seleccione</option>
    <?php foreach ($conciliators as $conciliator) { ?>
              <option value="<?= $conciliator->id?>">{{$conciliator->name}} {{$conciliator->lastname}}</option>
           <?php
        } ?>
    </select>
</div>
@endif


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('proceedings.index') }}" class="btn btn-default">Cancelar</a>
</div>
