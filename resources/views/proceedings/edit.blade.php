@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Expediente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($proceeding, ['route' => ['proceedings.update', $proceeding->id], 'method' => 'patch']) !!}

                        @include('proceedings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection