<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{{ $proceeding->number }}</p>
</div>

<!-- Application Filing Date Field -->
<div class="form-group">
    {!! Form::label('application_filing_date', 'Application Filing Date:') !!}
    <p>{{ $proceeding->application_filing_date }}</p>
</div>

<!-- Date Acceptance Conciliator Field -->
<div class="form-group">
    {!! Form::label('date_acceptance_conciliator', 'Date Acceptance Conciliator:') !!}
    <p>{{ $proceeding->date_acceptance_conciliator }}</p>
</div>

<!-- Date Acceptance Application Field -->
<div class="form-group">
    {!! Form::label('date_acceptance_application', 'Date Acceptance Application:') !!}
    <p>{{ $proceeding->date_acceptance_application }}</p>
</div>

<!-- Type Settlements Field -->
<div class="form-group">
    {!! Form::label('type_settlements', 'Type Settlements:') !!}
    <p>{{ $proceeding->type_settlements }}</p>
</div>

<!-- Arbitrament Field -->
<div class="form-group">
    {!! Form::label('arbitrament', 'Arbitrament:') !!}
    <p>{{ $proceeding->arbitrament }}</p>
</div>

<!-- Applicants Id Field -->
<div class="form-group">
    {!! Form::label('applicants_id', 'Applicants Id:') !!}
    <p>{{ $proceeding->applicants_id }}</p>
</div>

<!-- Summoneds Id Field -->
<div class="form-group">
    {!! Form::label('summoneds_id', 'Summoneds Id:') !!}
    <p>{{ $proceeding->summoneds_id }}</p>
</div>

<!-- Proceeding States Id Field -->
<div class="form-group">
    {!! Form::label('proceeding_states_id', 'Proceeding States Id:') !!}
    <p>{{ $proceeding->proceeding_states_id }}</p>
</div>

